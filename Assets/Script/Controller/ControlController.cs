﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlController : MonoBehaviour
{
    // Start is called before the first frame update

    public enum Controller
    {
        PC,
        Android
    };

    public enum CmdSpectatorList
    {
        Random = 0,
        Back = -1,
        Forward = 1
    };

    public Controller Control;

    public GameObject GameUI;

    public ControllerPlayer controllerPlayer;
    public CameraPlayer CameraLocalPlayer;

    public Player PlayerControlLocal;
    public Player PlayerControlSpec;

    public int IndexSpec = -1;

    // Update is called once per frame
    void Update()
    {
        if (PauseMenu.IsPauseMenu || Chat.IsChatActive)
        {
            controllerPlayer.ChangeTargetMove(Vector2.zero);

            if (PlayerControlLocal.weaponControl.IsFire)
                PlayerControlLocal.weaponControl.ClickButtonFireExit();

            return;
        }

        if(Control == Controller.PC && GameUI.activeInHierarchy)
        {
            if (PlayerControlLocal)
            {
                if (PlayerControlLocal.IsAlive)
                {
                    if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D))
                    {
                        float movengX = Input.GetAxis("Horizontal");
                        float movengY = Input.GetAxis("Vertical");

                        Vector2 movement = new Vector2(movengX > 0 || movengX < 0 ? movengX : 0, movengY > 0 || movengY < 0 ? movengY : 0);

                        controllerPlayer.ChangeTargetMove(movement);
                    }
                    else
                        controllerPlayer.ChangeTargetMove(Vector2.zero);

                    if (Input.GetButton("Fire1"))
                        PlayerControlLocal.weaponControl.ClickButtonFireEnter();
                    else if(Input.GetKey(KeyCode.R))
                        PlayerControlLocal.weaponControl.ClickButtonReloadEnter();
                    else
                    {
                        if (PlayerControlLocal.weaponControl.IsFire)
                            PlayerControlLocal.weaponControl.ClickButtonFireExit();
                    }

                    if (Input.GetKey(KeyCode.Alpha1))
                        PlayerControlLocal.weaponControl.ClickChangeWeapon(0);
                    else if (Input.GetKey(KeyCode.Alpha2))
                        PlayerControlLocal.weaponControl.ClickChangeWeapon(1);
                }
                else if(!PlayerControlLocal.IsAlive)
                {
                    if (Input.GetButtonDown("Fire1") && IndexSpec != -1)
                        LeftButtonSpect();
                    else if (Input.GetButtonDown("Fire2") && IndexSpec != -1)
                        RightButtonSpect();
                }

                if (!PlayerControlLocal.IsAlive && PlayerControlSpec != null)
                {
                    if (PlayerControlLocal.transform.position != PlayerControlSpec.transform.position)
                        PlayerControlLocal.transform.position = PlayerControlSpec.transform.position;
                }
            }
        }
    }

    public void SpectrMode()
    {
        if (PhysicalEntity.EntityLists.playerEnts == null || PhysicalEntity.EntityLists.playerEnts.Count <= 0)
            return;

        SpectrLogic((int)CmdSpectatorList.Random);
    }

    public void LeftButtonSpect()
    {
        if (PhysicalEntity.EntityLists.playerEnts == null || PhysicalEntity.EntityLists.playerEnts.Count <= 0)
            return;

        SpectrLogic((int)CmdSpectatorList.Back);
    }

    public void RightButtonSpect()
    {
        if (PhysicalEntity.EntityLists.playerEnts == null || PhysicalEntity.EntityLists.playerEnts.Count <= 0)
            return;

        SpectrLogic((int)CmdSpectatorList.Forward);
    }

    private void SpectrLogic(int DirectionForIndex)
    {
        if(DirectionForIndex == (int)CmdSpectatorList.Random)
            IndexSpec = Random.Range(0, PhysicalEntity.EntityLists.playerEnts.Count);
        else
            IndexSpec = IndexSpec + DirectionForIndex;

        if (IndexSpec >= PhysicalEntity.EntityLists.playerEnts.Count)
            IndexSpec = 0;
        else if (IndexSpec <= -1)
            IndexSpec = PhysicalEntity.EntityLists.playerEnts.Count - 1;

        if (PhysicalEntity.EntityLists.playerEnts[IndexSpec].Obj)
            PlayerControlSpec = PhysicalEntity.EntityLists.playerEnts[IndexSpec].PlayerProperties;

        int count = PhysicalEntity.EntityLists.playerEnts.Count - 1;

        while (PlayerControlSpec && !PlayerControlSpec.IsAlive && count > 0)
        {
            if (DirectionForIndex == (int)CmdSpectatorList.Random)
                IndexSpec = Random.Range(0, PhysicalEntity.EntityLists.playerEnts.Count);
            else
                IndexSpec = IndexSpec + DirectionForIndex;

            if (IndexSpec >= PhysicalEntity.EntityLists.playerEnts.Count)
                IndexSpec = 0;
            else if (IndexSpec <= -1)
                IndexSpec = PhysicalEntity.EntityLists.playerEnts.Count - 1;

            if (PhysicalEntity.EntityLists.playerEnts[IndexSpec].Obj)
                PlayerControlSpec = PhysicalEntity.EntityLists.playerEnts[IndexSpec].PlayerProperties;

            count--;
        }

        PlayerControlLocal.transform.position = PlayerControlSpec.transform.position;
        CameraLocalPlayer.ChangePlayerTarget(PlayerControlSpec.gameObject);
    }
}
