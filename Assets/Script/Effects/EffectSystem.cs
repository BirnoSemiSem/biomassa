using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class EffectSystem : NetworkBehaviour
{
    void OnEnable()
    {
        if(isServerOnly)
            return;
            
        GameObjStateControl.EventTakeDamage_Client += TakeDamage;
        GameObjStateControl.EventSpawnPlayer_Client += SpawnPlayer;
        GameObjStateControl.EventDeathPlayer_Client += DeathPlayer;

        Biohazard.EventInfectionPlayer += InfectionPlayer;
    }

    void OnDisable()
    {
        if(isServerOnly)
            return;

        GameObjStateControl.EventTakeDamage_Client -= TakeDamage;
        GameObjStateControl.EventSpawnPlayer_Client -= SpawnPlayer;
        GameObjStateControl.EventDeathPlayer_Client -= DeathPlayer;

        Biohazard.EventInfectionPlayer -= InfectionPlayer;
    }

    private void TakeDamage(Entity victim, Entity attacker, int damage, StatusFunc status)
    {
        if(status.GetStatus != StatusFuncReturn.StopAllSubscriberAndParent)
        {
            if (victim.Player.PlayerProperties != null)
            {
                Player _victim = victim.Player.PlayerProperties;

                if (attacker.Player.PlayerProperties != null)
                {
                    Player _attacker = attacker.Player.PlayerProperties;

                    if(_victim.PlayerTeam != _attacker.PlayerTeam)
                    {
                        StartCoroutine(ParticalBlood(victim.Player.Obj, _victim.IsAlive));
                    }
                }
            }
        }
    }

    private void DeathPlayer(Entity.PlayerEnt player, StatusFunc status)
    {
        if(status.GetStatus != StatusFuncReturn.StopAllSubscriberAndParent)
        {
            StartCoroutine(ParticalBlood(player.Obj, player.PlayerProperties.IsAlive));
        }
    }

    private void SpawnPlayer(Entity.PlayerEnt player, StatusFunc status)
    {
        if(status.GetStatus != StatusFuncReturn.StopAllSubscriberAndParent)
        {

        }
    }

    private void InfectionPlayer(Player player, int id)
    {
        StartCoroutine(ParticalInfection(player.gameObject));
    }

    public IEnumerator ParticalBlood(GameObject Player, bool IsAlive)
    {
        if (UnityEngine.Random.Range(0, 11) > 7)
        {
            GameObject _BloodOnFloor = OptimizationLogic.OptCode.GetPoolObject(OptimizationLogic.Effects.BloodFloor);
            _BloodOnFloor.transform.position = Player.transform.position + new Vector3(UnityEngine.Random.Range(-0.05f, 0.05f), UnityEngine.Random.Range(-0.1f, 0.1f), 0);
            _BloodOnFloor.transform.rotation = Quaternion.Euler(new Vector3(0, 0, UnityEngine.Random.Range(0, 360)));
            _BloodOnFloor.SetActive(true);
            _BloodOnFloor.GetComponent<AnimStartAndDestroyToTime>().Go();
        }

        if (UnityEngine.Random.Range(0, 11) > 7)
        {
            GameObject _BloodOnPain = OptimizationLogic.OptCode.GetPoolObject(OptimizationLogic.Effects.BloodPain);
            _BloodOnPain.transform.position = Player.transform.position + new Vector3(UnityEngine.Random.Range(-0.05f, 0.05f), UnityEngine.Random.Range(-0.1f, 0.1f), 0);
            _BloodOnPain.transform.rotation = Quaternion.Euler(new Vector3(0, 0, UnityEngine.Random.Range(0, 360)));
            _BloodOnPain.SetActive(true);
            _BloodOnPain.GetComponent<AnimStartAndDestroyToTime>().Go();
        }

        if(!IsAlive)
        {
            GameObject _BloodOnDeath = OptimizationLogic.OptCode.GetPoolObject(OptimizationLogic.Effects.Death);
            _BloodOnDeath.transform.position = Player.transform.position;
            _BloodOnDeath.transform.rotation = Quaternion.Euler(new Vector3(0, 0, UnityEngine.Random.Range(0, 360)));
            _BloodOnDeath.SetActive(true);
            _BloodOnDeath.GetComponent<AnimStartAndDestroyToTime>().Go();
        }

        yield return null;
    }

    public IEnumerator ParticalInfection(GameObject Player)
    {
        GameObject InfectionSpr = OptimizationLogic.OptCode.GetPoolObject(OptimizationLogic.Effects.Infection);
        InfectionSpr.transform.position = Player.transform.position + new Vector3(UnityEngine.Random.Range(-0.05f, 0.05f), UnityEngine.Random.Range(-0.1f, 0.1f), 0);
        InfectionSpr.transform.rotation = Quaternion.Euler(new Vector3(0, 0, UnityEngine.Random.Range(0, 360)));
        InfectionSpr.SetActive(true);
        InfectionSpr.GetComponent<AnimStartAndDestroyToTime>().Go();

        yield return null;
    }
}
