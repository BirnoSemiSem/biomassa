﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Biohazard : NetworkBehaviour
{
    [SerializeField] private KnockBack BioKnockback;

    [SerializeField] private float TimeToStart = 15f;
    [SerializeField] private float PreInfectionCountMulti = 0.15f;

    static private int PlayerCount = 0;
    static public int GetPlayerCount => PlayerCount;
    static private int ZombieCount = 0;
    static public int GetZombieCount => ZombieCount;

    static private int HumanCount = 0;
    static public int GetHumanCount => HumanCount;

    static public bool IsModBiohazardStart = false;

    static public GameObject BotLeaderZombie;
    static public GameObject BotLeaderHuman;

    static public bool IsBotHuntForZombies = false;
    static public bool IsBotHuntForHumans = false;

    static public bool IsBotGroupZombie = false;
    static public bool IsBotGroupHuman = false;

    [SerializeField] private ZombieClassSystem[] ZombieClassParametrs;
    [SerializeField] private HumanClassSystem[] HumanClassParametrs;

    private Coroutine InfectionTimer;

    [SerializeField] private float CurrentTime = 0f;

    //Delegate
    static public UnityAction EventBiohazardStart;
    static public UnityAction EventHumanWin;
    static public UnityAction EventZombieWin;
    static public UnityAction<Player, int> EventInfectionPlayer;

    [SerializeField] private bool IsDebug = false;
    [SerializeField] private float DebugKnockbackPower = 1f;
    [SerializeField] private float DebugKnockbackTime = 1f;

    void Start()
    {
        Initialization();
    }

    public override void OnStartClient()
    {
        base.OnStartClient();
        Initialization();
    }

    public override void OnStartServer()
    {
        base.OnStartServer();
        Initialization();
    }

    void Initialization()
    {
        BotLeaderZombie = null;
        BotLeaderHuman = null;

        IsModBiohazardStart = false;

        IsBotHuntForZombies = false;
        IsBotHuntForHumans = false;

        IsBotGroupZombie = false;
        IsBotGroupHuman = false;

        PlayerCount = 0;
        ZombieCount = 0;
        HumanCount = 0;

        CurrentTime = 0;
    }

    void OnEnable()
    {
        GameControl.EventRoundStart += StartingTimerPreInfecton;
        GameControl.EventRoundEnd += EndRound;

        GameObjStateControl.EventTakeDamage_Before += TakeDamagePlayer_Before;
        GameObjStateControl.EventSpawnPlayer_After += PlayerSpawn;
        GameObjStateControl.EventDeathPlayer_After += PlayerDeath;

        Player.EventsPlayerStart += UpdatePlayerProperties;
    }

    void OnDisable()
    {
        GameControl.EventRoundStart -= StartingTimerPreInfecton;
        GameControl.EventRoundEnd -= EndRound;

        GameObjStateControl.EventTakeDamage_Before -= TakeDamagePlayer_Before;
        GameObjStateControl.EventSpawnPlayer_After -= PlayerSpawn;
        GameObjStateControl.EventDeathPlayer_After -= PlayerDeath;

        Player.EventsPlayerStart -= UpdatePlayerProperties;
    }

    void Update()
    {
        CountsPlayers();

        if (isServer)
        {
            if (PhysicalEntity.EntityLists.playerEnts == null)
                return;

            if (IsBotHuntForZombies)
                HunterStarting();

            CurrentTime += Time.deltaTime;

            if (!GameControl.GetIsEndRound && CurrentTime > (TimeToStart + 5f))
            {
                if (IsModBiohazardStart && !GameControl.IsForcedEndRound && GameControl.GetIsGame)
                {
                    if ((ZombieCount <= 0 || HumanCount <= 0) && PlayerCount >= 2)
                    {
                        GameControl.IsForcedEndRound = true;

                        if (ZombieCount <= 0)
                            RoundHumanWin();
                        else if (HumanCount <= 0)
                            RoundZombieWin();
                    }
                }
                else if (!IsModBiohazardStart && !GameControl.IsForcedEndRound)
                {
                    if (PlayerCount >= 2 && GameControl.GetIsGame)
                    {
                        GameControl.IsForcedEndRound = true;
                    }
                }
            }
        }
    }

    void CountsPlayers()
    {
        PlayerCount = PhysicalEntity.EntityLists.playerEnts.Count;

        for (int i = 0, Zcount = 0, Hcount = 0; i < PlayerCount; i++)
        {
            if (PhysicalEntity.EntityLists.playerEnts[i].Obj && PhysicalEntity.EntityLists.playerEnts[i].PlayerProperties.IsAlive)
            {
                switch (PhysicalEntity.EntityLists.playerEnts[i].PlayerProperties.PlayerTeam)
                {
                    case Player.Team.Zombie: { Zcount++; break; }
                    case Player.Team.Human: { Hcount++; break; }
                }
            }

            ZombieCount = Zcount;
            HumanCount = Hcount;
        }
    }

    public void RoundHumanWin()
    {
        RpcRoundHumanWin();
        EventHumanWin?.Invoke();
    }

    public void RoundZombieWin()
    {
        RpcRoundZombieWin();
        EventZombieWin?.Invoke();
    }

    public void StartingTimerPreInfecton()
    {
        if (!isServer)
            return;

        CurrentTime = 0f;

        if (!IsModBiohazardStart)
        {
            if(InfectionTimer != null)
                StopCoroutine(InfectionTimer);

            InfectionTimer = StartCoroutine(StartInfection());
        }

        BotLeaderHuman = null;
        BotLeaderZombie = null;

        IsBotHuntForZombies = false;
        IsBotHuntForHumans = false;

        IsBotGroupZombie = false;
        IsBotGroupHuman = false;
    }

    private void TakeDamagePlayer_Before(Entity victim, Entity attacker, int damage, StatusFunc status)
    {
        if (victim.Player.PlayerProperties)
        {
            Player _victim = victim.Player.PlayerProperties;

            if (attacker.Player.PlayerProperties)
            {
                Player _attacker = attacker.Player.PlayerProperties;

                if (Biohazard.IsModBiohazardStart)
                {
                    switch (_attacker.PlayerTeam)
                    {
                        case Player.Team.Zombie:
                        {
                            if (_victim.PlayerTeam == Player.Team.Human)
                            {
                                if (HumanCount > 1)
                                {
                                    if (_victim.Armor > 0)
                                        _victim.Armor -= damage;
                                    else
                                    {
                                        RpcInfection(victim.Player.PlayerProperties);
                                        StartCoroutine(InfectionPlayer(victim.Player.PlayerProperties));
                                    }
                                }
                                else
                                {
                                    if(_victim.Armor > 0)
                                        _victim.Armor -= damage;
                                    else
                                        _victim.Health -= damage;
                                }
                            }

                            break;
                        }
                        case Player.Team.Human:
                        {
                            if (_victim.PlayerTeam == Player.Team.Zombie)
                            {
                                _victim.Health -= damage;

                                if(IsDebug)
                                {
                                    Debug.Log($"DebugKnockbackTime: {DebugKnockbackTime} | DebugKnockbackPower: {DebugKnockbackPower} " +
                                    $"| Zombie Knockback: {_victim.ZombieClass.KnockBack} | Weapon: {_attacker.Weapons[_attacker.ActiveWeapon].Knockback} | Sum: {DebugKnockbackPower * _victim.ZombieClass.KnockBack * _attacker.Weapons[_attacker.ActiveWeapon].Knockback}");
                                    BioKnockback.TakeKnockBack(DebugKnockbackTime, DebugKnockbackPower* _victim.ZombieClass.KnockBack * _attacker.Weapons[_attacker.ActiveWeapon].Knockback, victim.Player.Obj, attacker.Player.Obj); // NOOO! BUG!!!

                                }
                                else
                                    BioKnockback.TakeKnockBack(0.15f, _victim.ZombieClass.KnockBack * _attacker.Weapons[_attacker.ActiveWeapon].Knockback, victim.Player.Obj, attacker.Player.Obj); // NOOO! BUG!!!
                            }

                            break;
                        }
                    }

                    if (_victim.Health <= 0 && _victim.IsAlive)
                    {
                        GameObjStateControl.gameObjStateControl.GOSC_KillObj(victim, attacker);
                    }
                }
            }
        }
        status.SetStatus(StatusFuncReturn.StopAllSubscriberAndParent);
    }

    private void PlayerSpawn(Entity.PlayerEnt PlayerEnt, StatusFunc status)
    {
        StartCoroutine(PlayerDelay(PlayerEnt.PlayerProperties));
    }

    private void PlayerDeath(Entity.PlayerEnt PlayerEnt, StatusFunc status)
    {
        if (!isServer)
            return;

        if (IsModBiohazardStart)
        {
            Entity ent = new Entity(PlayerEnt.id, PlayerEnt.Obj, PlayerEnt.PlayerProperties);
            GameObjStateControl.gameObjStateControl.GOSC_SpawnObj(ent, 3f, Player.Team.Zombie);
        }

        CancelInvoke(nameof(HunterStarting));
        Invoke(nameof(HunterStarting), Random.Range(20f, 30f));
    }

    public void EndRound()
    {
        IsModBiohazardStart = false;

        BotLeaderHuman = null;
        BotLeaderZombie = null;

        IsBotHuntForZombies = false;
        IsBotHuntForHumans = false;

        IsBotGroupZombie = false;
        IsBotGroupHuman = false;

        if(InfectionTimer != null)
            StopCoroutine(InfectionTimer);

        if(isServer && HumanCount > 0)
            RoundHumanWin();
    }

    private IEnumerator PlayerDelay(Player PlayerEnt)
    {
        if (PlayerEnt.IsAlive)
        {
            switch (PlayerEnt.PlayerTeam)
            {
                case global::Player.Team.Zombie:
                {
                    if (!isServer)
                        yield break;

                    RpcInfection(PlayerEnt);
                    StartCoroutine(InfectionPlayer(PlayerEnt));
                    break;
                }
                case global::Player.Team.Human:
                {
                    if(isServer)
                        PlayerEnt.HumanSelect = Random.Range(0, HumanClassParametrs.Length);

                    yield return new WaitForSeconds(0.1f);

                    if (PlayerEnt == null)
                        yield break;

                    PlayerClassParametrs(PlayerEnt);
                    PlayerSoundUpdate(PlayerEnt);

                    PlayerEnt.Health = PlayerEnt.HumanClass.Health;
                    PlayerEnt.Armor = PlayerEnt.HumanClass.Armor;

                    if (PlayerEnt.Animator != null)
                    {
                        PlayerEnt.Animator.SetInteger("HumanClass", PlayerEnt.HumanSelect);

                        if (PlayerEnt.Weapons.Length > 0)
                        {
                            Player.AnimationPlayer animationPlayer = (Player.AnimationPlayer)PlayerEnt.ActiveWeapon + 1;
                            AnimationObj.AnimationPlayer(PlayerEnt, PlayerEnt.HumanClass.Name + "-" + animationPlayer.ToString());
                        }
                        else
                        {
                            AnimationObj.AnimationPlayer(PlayerEnt, PlayerEnt.HumanClass.Name + "-" + global::Player.AnimationPlayer.Idle.ToString());
                        }
                    }

                    PlayerEnt.IsFirstZombie = false;
                    PlayerEnt.PlayerSprite.color = new Color(255f, 255f, 255f, 255f);
                    break;
                }
            }
        }
    }

    public IEnumerator StartInfection()
    {
        yield return new WaitForSeconds(TimeToStart);

        if (PlayerCount >= 2)
        {
            // GameControl.IsStartRound = false;
            IsModBiohazardStart = true;

            if (isServer)
            {
                int CountFirstZombie = Mathf.CeilToInt(HumanCount * PreInfectionCountMulti);
                int[] IndexFirstZombie = new int[CountFirstZombie];

                int i = 0;
                for (i = 0; i < CountFirstZombie; i++)
                {
                    IndexFirstZombie[i] = Random.Range(0, HumanCount);

                    for (int j = 0; j < i; j++)
                    {
                        if (IndexFirstZombie[i] == IndexFirstZombie[j])
                        {
                            i--;
                            break;
                        }
                    }

                    PhysicalEntity.EntityLists.playerEnts[IndexFirstZombie[i]].PlayerProperties.IsFirstZombie = true;
                }

                yield return new WaitForSeconds(0.25f);

                for (i = 0; i < PhysicalEntity.EntityLists.playerEnts.Count; i++)
                {
                    if (PhysicalEntity.EntityLists.playerEnts[i].PlayerProperties.IsFirstZombie)
                    {
                        RpcInfection(PhysicalEntity.EntityLists.playerEnts[i].PlayerProperties);
                        StartCoroutine(InfectionPlayer(PhysicalEntity.EntityLists.playerEnts[i].PlayerProperties));
                    }
                }
            }

            EventBiohazardStart?.Invoke();
        }
    }

    public IEnumerator InfectionPlayer(Player PlayerEnt)
    {
        if (!PlayerEnt)
            yield break;

        if (IsModBiohazardStart)
        {
            if(isServer)
                PlayerEnt.ZombieSelect = Random.Range(0, ZombieClassParametrs.Length);

            yield return new WaitForSeconds(0.1f);

            PlayerEnt.PlayerTeam = global::Player.Team.Zombie;
            PlayerEnt.ActiveWeapon = 0;

            PlayerClassParametrs(PlayerEnt);
            PlayerSoundUpdate(PlayerEnt);

            if (PlayerEnt.IsFirstZombie)
                PlayerEnt.Health = PlayerEnt.ZombieClass.Health * 2;
            else
                PlayerEnt.Health = PlayerEnt.ZombieClass.Health;

            PlayerEnt.Armor = 0;

            WeaponReset(PlayerEnt, ZombieClassParametrs[PlayerEnt.ZombieSelect].ZombieKnife);

            if (PlayerEnt.Animator != null)
            {
                PlayerEnt.Animator.SetInteger("ZombieClass", PlayerEnt.ZombieSelect);
                AnimationObj.AnimationPlayer(PlayerEnt, PlayerEnt.ZombieClass.Name + "-" + global::Player.AnimationPlayer.Idle.ToString());
            }

            if (!isServerOnly)
            {
                GameObject InfectionSpr = OptimizationLogic.OptCode.GetPoolObject(OptimizationLogic.Effects.Infection);
                InfectionSpr.transform.position = PlayerEnt.transform.position + new Vector3(Random.Range(-0.05f, 0.05f), Random.Range(-0.1f, 0.1f), 0);
                InfectionSpr.transform.rotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(0, 360)));
                InfectionSpr.SetActive(true);
                InfectionSpr.GetComponent<AnimStartAndDestroyToTime>().Go();
            }

            EventInfectionPlayer?.Invoke(PlayerEnt, PlayerEnt.GetPlayerID);

            if (isServer)
            {
                CancelInvoke(nameof(HunterStarting));
                Invoke(nameof(HunterStarting), Random.Range(20f, 30f));
            }

            yield return null;
        }
    }

    public void HunterStarting()
    {
        if(IsModBiohazardStart)
        {
            if (ZombieCount < HumanCount * 0.25f)
            {
                IsBotHuntForZombies = true;
            }
            else
                IsBotHuntForZombies = false;
        }
    }

    public void Antidot(GameObject PlayerEnt)
    {
        if(IsModBiohazardStart)
        {

        }
    }

    public void WeaponReset(Player PlayerEnt, WeaponSystem ZombieKnife)
    {
        if (PlayerEnt == null)
            return;

        PlayerEnt.Weapons = null;
        PlayerEnt.Weapons = new Player.WeaponPlayer[3];

        if(PlayerEnt.WeaponSprite)
            PlayerEnt.WeaponSprite.sprite = null;

        PlayerEnt.Weapons[0].Weapon = ZombieKnife.Weapon;

        PlayerEnt.Weapons[0].NameWeapon = ZombieKnife.NameWeapon;

        PlayerEnt.Weapons[0].MinDmg = ZombieKnife.MinDmg;
        PlayerEnt.Weapons[0].MaxDmg = ZombieKnife.MaxDmg;
        PlayerEnt.Weapons[0].HSDmg = ZombieKnife.HSDmg;

        PlayerEnt.KnifeMissClips = ZombieKnife.KnifeMissClips;
        PlayerEnt.KnifeStrikeClips = ZombieKnife.KnifeStrikeClips;
    }

    public void UpdatePlayerProperties(GameObject PlayerObject)
    {
        Player PlayerEnt = PlayerObject.GetComponent<Player>();

        PlayerClassParametrs(PlayerEnt);
        PlayerSoundUpdate(PlayerEnt);
    }

    void PlayerClassParametrs(Player PlayerEnt)
    {
        switch (PlayerEnt.PlayerTeam)
        {
            case Player.Team.Zombie:
            {
                if (ZombieClassParametrs != null)
                {
                    PlayerEnt.ZombieClass.Name = ZombieClassParametrs[PlayerEnt.ZombieSelect].Name;
                    PlayerEnt.ZombieClass.Health = ZombieClassParametrs[PlayerEnt.ZombieSelect].Health;
                    PlayerEnt.ZombieClass.Speed = ZombieClassParametrs[PlayerEnt.ZombieSelect].Speed;
                    PlayerEnt.ZombieClass.KnockBack = ZombieClassParametrs[PlayerEnt.ZombieSelect].KnockBack;
                    PlayerEnt.ZombieClass.RegerationHP = ZombieClassParametrs[PlayerEnt.ZombieSelect].RegerationHP;
                    PlayerEnt.ZombieClass.RegerationSpeed = ZombieClassParametrs[PlayerEnt.ZombieSelect].RegerationSpeed;
                    PlayerEnt.ZombieClass.PainShock = ZombieClassParametrs[PlayerEnt.ZombieSelect].PainShock;
                    PlayerEnt.ZombieClass.PainShockSpeed = ZombieClassParametrs[PlayerEnt.ZombieSelect].PainShockSpeed;
                }

                break;
            }
            case Player.Team.Human:
            {
                if (HumanClassParametrs != null)
                {
                    PlayerEnt.HumanClass.Name = HumanClassParametrs[PlayerEnt.HumanSelect].Name;
                    PlayerEnt.HumanClass.Health = HumanClassParametrs[PlayerEnt.HumanSelect].Health;
                    PlayerEnt.HumanClass.Armor = HumanClassParametrs[PlayerEnt.HumanSelect].Armor;
                    PlayerEnt.HumanClass.Speed = HumanClassParametrs[PlayerEnt.HumanSelect].Speed;
                    PlayerEnt.HumanClass.PainShock = HumanClassParametrs[PlayerEnt.HumanSelect].PainShock;
                    PlayerEnt.HumanClass.PainShockSpeed = HumanClassParametrs[PlayerEnt.HumanSelect].PainShockSpeed;
                }

                break;
            }
        }
    }

    void PlayerSoundUpdate(Player PlayerEnt)
    {
        switch (PlayerEnt.PlayerTeam)
        {
            case Player.Team.Zombie:
            {
                PlayerEnt.FeetClips = ZombieClassParametrs[PlayerEnt.ZombieSelect].FeetClips;
                PlayerEnt.DeathClips = ZombieClassParametrs[PlayerEnt.ZombieSelect].DeathClips;
                PlayerEnt.PainClips = ZombieClassParametrs[PlayerEnt.ZombieSelect].PainClips;
                PlayerEnt.InfectionClips = ZombieClassParametrs[PlayerEnt.ZombieSelect].InfectionClips;

                break;
            }
            case Player.Team.Human:
            {
                PlayerEnt.FeetClips = HumanClassParametrs[PlayerEnt.HumanSelect].FeetClips;
                PlayerEnt.DeathClips = HumanClassParametrs[PlayerEnt.HumanSelect].DeathClips;
                PlayerEnt.PainClips = HumanClassParametrs[PlayerEnt.HumanSelect].PainClips;

                break;
            }
        }
    }

    [ClientRpc]
    public void RpcRoundHumanWin()
    {
        if (isServer)
            return;

        EventHumanWin?.Invoke();
    }

    [ClientRpc]
    public void RpcRoundZombieWin()
    {
        if (isServer)
            return;

        EventZombieWin?.Invoke();
    }

    [ClientRpc]
    public void RpcInfection(Player Player)
    {
        if (isServer)
            return;

        if (!IsModBiohazardStart)
            IsModBiohazardStart = true;

        StartCoroutine(InfectionPlayer(Player));
    }
}
