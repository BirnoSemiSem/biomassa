using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
using UnityEngine.UI;

public class GameControl : NetworkBehaviour
{
    // Start is called before the first frame update

    [SerializeField] public static List<Transform> Spawns {get; private set; } = new List<Transform>();
    [SerializeField] private float RoundTimeMinut = 5f;
    [SyncVar] [SerializeField] private float RoundTime;
    [SerializeField] private int FreezeTime= 5;
    [SerializeField] private float RoundTimeEnd = 0;
    [SerializeField] private int TimeEndRound = 3;

    static private bool IsNewRound = false;
    static public bool GetIsNewRound => IsNewRound;
    static private bool IsFreeze = false;
    static public bool GetIsFreeze => IsFreeze;
    static private bool IsStartRound = false;
    static public bool GetIsStartRound => IsStartRound;
    static private bool IsEndRound = false;
    static public bool GetIsEndRound => IsEndRound;
    static private bool IsGame = false;
    static public bool GetIsGame => IsGame;
    static public bool IsForcedEndRound = false;

    static private bool IsInfinityAmmo = true;
    static public bool GetIsInfinityAmmo => IsInfinityAmmo;
    static private bool IsRespawnInGame = true;
    static public bool GetIsRespawnInGame => IsRespawnInGame;

    static private int TextTimeMinut;
    static public int GetTextTimeMinut => TextTimeMinut;
    static private int TextTimeSecond;
    static public int GetTextTimeSecond => TextTimeSecond;

    [SerializeField] private GameObject BotPlayer;
    [SerializeField] private GameObject BotParent;

    [SerializeField] private GameObject PlayerParent;

    static public List<GameObject> Bots = new List<GameObject>();

    [SerializeField] private int BotQuota = 0;

    //Delegate
    static public UnityAction EventPreStartRound;
    static public UnityAction EventRoundStart;
    static public UnityAction EventRoundEnd;

    void Start()
    {
        Application.targetFrameRate = 120;

        Initialization();
    }

    public override void OnStartClient()
    {
        base.OnStartClient();
        Initialization();
    }

    public override void OnStartServer()
    {
        base.OnStartServer();
        Initialization();
    }

    void Initialization()
    {
        GameObject[] FindSpawns = GameObject.FindGameObjectsWithTag("Spawn");

        if(Spawns.Count <= 0)
        {
            for (int i = 0; i < FindSpawns.Length; i++)
                Spawns.Add(FindSpawns[i].transform);
        }

        if(BotQuota <= 0)
            BotQuota = SpecificalOptions.specificalOptions.ServerOptions.CountBots;

        RoundTime = 0;

        IsNewRound = false;
        IsFreeze = false;
        IsStartRound = false;
        IsEndRound = false;
        IsForcedEndRound = true;
        IsGame = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (isServer)
        {
            if (RoundTime <= 0 && RoundTimeEnd <= 0 && IsEndRound)
            {
                RoundTime = FreezeTime;
                NewRound();
            }
            else
            if ((IsNewRound && RoundTime <= 0) || IsForcedEndRound)
            {
                if (IsForcedEndRound)
                {
                    RoundTimeEnd = TimeEndRound;
                    RoundTime = 0f;
                    GameEnd();
                }
                else
                if (IsFreeze)
                {
                    RoundTime = 60f * RoundTimeMinut;
                    GameStart();
                }
                else
                if (IsGame)
                {
                    RoundTimeEnd = TimeEndRound;
                    GameEnd();
                }
            }
            else
            {
                if (IsEndRound && RoundTimeEnd >= 0)
                    RoundTimeEnd -= Time.deltaTime;
                else
                    RoundTime -= Time.deltaTime;
            }
        }

        TextTimeMinut = (int)(RoundTime / 60);
        TextTimeSecond = (int)(RoundTime % 60);
    }

    public void NewRound()
    {
        if (isServer)
            RpcNewRound();

        IsNewRound = true;
        IsFreeze = true;
        IsEndRound = false;

        IsRespawnInGame = true;

        EventPreStartRound?.Invoke();

        if (PhysicalEntity.EntityLists.playerEnts != null)
        {
            for (int i = 0; i < PhysicalEntity.EntityLists.playerEnts.Count; i++)
            {
                if (PhysicalEntity.EntityLists.playerEnts[i].PlayerProperties.PlayerTeam != global::Player.Team.Spectator)
                {
                    if (isServer)
                    {
                        PhysicalEntity.EntityLists.playerEnts[i].PlayerProperties.RandomSpawn = UnityEngine.Random.Range(0, Spawns.Count);
                    }

                    
                    Entity ent = new Entity(PhysicalEntity.EntityLists.playerEnts[i].id, PhysicalEntity.EntityLists.playerEnts[i].Obj, PhysicalEntity.EntityLists.playerEnts[i].PlayerProperties);
                    GameObjStateControl.gameObjStateControl.GOSC_SpawnObj(ent, 0.1f, Player.Team.Human);
                }
            }
        }
    }

    public void GameStart()
    {
        if (isServer)
            RpcGameStart();

        IsStartRound = true;
        IsFreeze = false;
        IsGame = true;

        EventRoundStart?.Invoke();
    }

    public void GameEnd()
    {
        if(isServer)
            RpcGameEnd();

        StopAllCoroutines();

        IsEndRound = true;
        IsStartRound = false;
        IsGame = false;
        IsNewRound = false;
        IsFreeze = false;
        IsForcedEndRound = false;

        IsRespawnInGame = false;

        BotUpdateCount();

        EventRoundEnd?.Invoke();
    }

    // public void PlayerDeath(GameObject Player, int id)
    // {
    //     if (!isServer)
    //         return;

    //     if (!IsRespawnInGame)
    //         return;

    //     if (IsGame && Biohazard.IsModBiohazardStart)
    //     {
    //         NetworkIdentity opponentIdentity = Player.GetComponent<NetworkIdentity>();

    //         if (opponentIdentity.connectionToClient != null)
    //             TargetRespawn(opponentIdentity.connectionToClient, Player);

    //         StartCoroutine(RespawnPlayer(Player, 5f, global::Player.Team.Zombie));
    //     }
    // }

    // public void WeaponUpdate(GameObject PlayerObject)
    // {
    //     Player _player = PlayerObject.GetComponent<Player>();

    //     if (!_player.IsAlive)
    //         return;

    //     switch(_player.PlayerTeam)
    //     {
    //         case Player.Team.Zombie:
    //         {
    //             _player.Weapons = null;
    //             _player.Weapons = new Player.WeaponPlayer[1];

    //             _player.WeaponSprite.sprite = null;

    //             if(ZombieKnifeParametrs != null)
    //                 SetWeapon(_player, 0, ZombieKnifeParametrs.GetComponent<WeaponSystem>());

    //             break;
    //         }
    //         case Player.Team.Human:
    //         {
    //             if(PrimaryWeaponParametrs != null)
    //                 SetWeapon(_player, 0, PrimaryWeaponParametrs[_player.RandomWeaponPrimary].GetComponent<WeaponSystem>());

    //             if(SecondWeaponParametrs != null)
    //                 SetWeapon(_player, 1, SecondWeaponParametrs[_player.RandomWeaponSecond].GetComponent<WeaponSystem>());

    //             //SetWeapon(_player, 2, GrenadeWeapon[_player.RandomWeaponGrenade].GetComponent<WeaponSystem>());

    //             break;
    //         }
    //     }
    // }

    // public void UpdateWeapon()
    // {
    //     switch(Biohazard.Mode)
    //     {
    //         case Biohazard.BiohazardMode.Classic:
    //         {
    //             PrimaryWeaponParametrs = ClassicPrimaryWeapon;
    //             SecondWeaponParametrs = ClassicSecondWeapon;
    //             GrenadeWeaponParametrs = ClassicGrenadeWeapon;
    //             KnifeWeaponParametrs = ClassicKnifeWeapon;

    //             ZombieKnifeParametrs = ClassicZombieKnife;

    //             break;
    //         }
    //         case Biohazard.BiohazardMode.New:
    //         {
    //             PrimaryWeaponParametrs = NewPrimaryWeapon;
    //             SecondWeaponParametrs = NewSecondWeapon;
    //             GrenadeWeaponParametrs = NewGrenadeWeapon;
    //             KnifeWeaponParametrs = NewKnifeWeapon;

    //             ZombieKnifeParametrs = NewZombieKnife;

    //             break;
    //         }
    //     }
    // }

    void BotUpdateCount()
    {
        if (isServer)
        {
            if (Bots.Count < BotQuota)
            {
                int NewCountBots = BotQuota - Bots.Count;
                for (int i = 0; i < NewCountBots; i++)
                {
                    GameObject NewBot = Instantiate(BotPlayer);
                    NetworkServer.Spawn(NewBot);
                    NewBot.transform.parent = BotParent.transform;

                    int SpawnIndex = UnityEngine.Random.Range(0, Spawns.Count);

                    NewBot.GetComponent<NavMeshAgent>().nextPosition = new UnityEngine.Vector2(Spawns[SpawnIndex].position.x, Spawns[SpawnIndex].position.y);
                    NewBot.transform.position = new UnityEngine.Vector2(Spawns[SpawnIndex].position.x, Spawns[SpawnIndex].position.y);

                    NewBot.GetComponent<NavMeshAgent>().enabled = false;
                    NewBot.GetComponent<NavMeshAgent>().enabled = true;

                    NewBot.GetComponent<BotControl>().Bot = BotControl.BotStatus.IsStoping;

                    Bots.Add(NewBot);

                    RpcCreateBot(NewBot, SpawnIndex);
                }
            }
            else if(Bots.Count > BotQuota)
            {
                int countDestroy = Bots.Count - BotQuota;

                for (int i = 0; i < countDestroy; i++)
                {
                    NetworkServer.Destroy(Bots[0]);
                    Bots.RemoveAt(0);

                }

                RpcDestroyBots(countDestroy);
            }
        }
    }

    public void ConnectedRespawn(GameObject PlayerObject)
    {
        if (isServer)
        {
            NetworkIdentity opponentIdentity = PlayerObject.GetComponent<NetworkIdentity>();

            if (opponentIdentity.connectionToClient != null)
                TargetConnectedRespawn(opponentIdentity.connectionToClient, PlayerObject, IsRespawnInGame);
        }

        Player _player = PlayerObject.GetComponent<Player>();

        if(isServer)
        {
            _player.RandomSpawn = UnityEngine.Random.Range(0, Spawns.Count);

            // _player.RandomWeaponPrimary = UnityEngine.Random.Range(0, PrimaryWeaponParametrs.Length);
            // _player.RandomWeaponSecond = UnityEngine.Random.Range(0, SecondWeaponParametrs.Length);
            // _player.RandomWeaponGrenade = UnityEngine.Random.Range(0, GrenadeWeaponParametrs.Length);
        }

        if (_player.IsAlive)
            return;

        Entity ent = new Entity(_player.GetPlayerID, _player.gameObject, _player);

        if (IsGame && Biohazard.IsModBiohazardStart)
        {
            if (IsRespawnInGame)
                GameObjStateControl.gameObjStateControl.GOSC_SpawnObj(ent, 3f, Player.Team.Zombie);

        }
        else
            GameObjStateControl.gameObjStateControl.GOSC_SpawnObj(ent, 3f, Player.Team.Human);

    }

    [ClientRpc]
    public void RpcNewRound()
    {
        if (isServer)
            return;

        NewRound();
    }

    [ClientRpc]
    void RpcCreateBot(GameObject Bot, int SpawnIndex)
    {
        if (isServer)
            return;

        Bot.transform.parent = BotParent.transform;
        Bot.transform.position = Spawns[SpawnIndex].position;

        Bot.GetComponent<NavMeshAgent>().enabled = false;

        Bots.Add(Bot);
    }

    [ClientRpc]
    void RpcDestroyBots(int countDestroy)
    {
        if (isServer)
            return;

        if(countDestroy <= Bots.Count)
        {
            for (int i = 0; i < countDestroy; i++)
            {
                Bots.RemoveAt(0);
            }
        }
    }

    [ClientRpc]
    public void RpcGameStart()
    {
        if (isServer)
            return;

        GameStart();
    }

    [ClientRpc]
    public void RpcGameEnd()
    {
        if (isServer)
            return;

        GameEnd();
    }

    [TargetRpc]
    void TargetConnectedRespawn(NetworkConnection target, GameObject Player, bool IsRespawn)
    {
        if (isServer)
            return;

        IsRespawnInGame = IsRespawn;
        ConnectedRespawn(Player);
    }

    [TargetRpc]
    void TargetRespawn(NetworkConnection target, GameObject Player)
    {
        if (isServer)
            return;

        Player _player = Player.GetComponent<Player>();

        if (_player.IsAlive)
            return;

        Entity ent = new Entity(_player.GetPlayerID, _player.gameObject, _player);

        GameObjStateControl.gameObjStateControl.GOSC_SpawnObj(ent, 5f, global::Player.Team.Zombie);
    }
}
