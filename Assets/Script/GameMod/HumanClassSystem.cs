﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanClassSystem : MonoBehaviour
{
    public string Name;
    public int Health;
    public int Armor;
    public float Speed;
    public float PainShock;
    public float PainShockSpeed;

    public AudioClip[] FeetClips;
    public AudioClip[] DeathClips;
    public AudioClip[] PainClips;
}
