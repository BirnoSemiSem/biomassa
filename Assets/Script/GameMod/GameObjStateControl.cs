using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.Events;

public enum StatusFuncReturn
{
    Continue,                       // Continues in all subscribers
    StopParent,                     // Ignoring parent
    StopAllSubscriberAndParent      // Extreme case!!! Stop for all other subscriber and parent
}

public class StatusFunc : EventArgs
{
    private StatusFuncReturn Status;

    public StatusFunc() => Status = StatusFuncReturn.Continue;

    public void SetStatus(StatusFuncReturn s)
    {
        if(Status != StatusFuncReturn.StopAllSubscriberAndParent) // Extreme case!!!
            Status = s;
        else
            Debug.LogWarning($"{nameof(Status)} is {nameof(StatusFuncReturn.StopAllSubscriberAndParent)}");
    }

    public StatusFuncReturn GetStatus => Status;
}


public class GameObjStateControl : NetworkBehaviour
{
    public static GameObjStateControl gameObjStateControl;

    //Delegate
    static public UnityAction<Entity, Entity, int, StatusFunc> EventTakeDamage_Before;  // Server
    static public UnityAction<Entity, Entity, int, StatusFunc> EventTakeDamage_After;   // Server
    static public UnityAction<Entity, Entity, int, StatusFunc> EventTakeDamage_Client;  // Client

    static public UnityAction<Entity.PlayerEnt, StatusFunc> EventSpawnPlayer_Before;   // Server
    static public UnityAction<Entity.PlayerEnt, StatusFunc> EventSpawnPlayer_After;    // Server
    static public UnityAction<Entity.PlayerEnt, StatusFunc> EventSpawnPlayer_Client;   // Client
    static public UnityAction<Entity.PlayerEnt, StatusFunc> EventDeathPlayer_Before;   // Server
    static public UnityAction<Entity.PlayerEnt, StatusFunc> EventDeathPlayer_After;    // Server
    static public UnityAction<Entity.PlayerEnt, StatusFunc> EventDeathPlayer_Client;   // Client

    void Start()
    {
        if(gameObjStateControl == null)
            gameObjStateControl = this;
    }

    public void GOSC_TakeDamageObj(Entity victim, Entity attacker, int damage)
    {
        if (!isServer)
            return;

        if(victim == null)
        {
            Debug.LogError($"{ nameof(victim) } is null!");
            return;
        }

        if(attacker == null)
        {
            Debug.LogError($"{ nameof(attacker) } is null!");
            return;
        }

        StartCoroutine(TakeDamegeObj(victim, attacker, damage));
    }

    public void GOSC_SpawnObj(Entity Obj, float timer = 0.0f, Player.Team team = Player.Team.None)
    {
        if (!isServer)
            return;

        if(Obj == null)
        {
            Debug.LogError($"{ nameof(Obj) } is null!");
            return;
        }

        if(team != Player.Team.None)
            StartCoroutine(SpawnPlayer(Obj.Player, timer, team));
    }

    public void GOSC_KillObj(Entity victim, Entity attacker)
    {
        if (!isServer)
            return;

        if(victim == null)
        {
            Debug.LogError($"{ nameof(victim) } is null!");
            return;
        }

        if(attacker == null)
            Debug.LogWarning($"{ nameof(attacker) } is null!");

        if(victim.Player != null)
        {
            if (!victim.Player.PlayerProperties.IsAlive)
            {
                Debug.LogError($"{ nameof(attacker) } is not alive!");
                return;
            }

            if (victim.Player.PlayerProperties)
            {
                StartCoroutine(DeathPlayer(victim.Player));
            }
        }
    }

    private IEnumerator TakeDamegeObj(Entity victim, Entity attacker, int damage)
    {
        StatusFunc status = new StatusFunc();
        EventTakeDamage_Before?.Invoke(victim, attacker, damage, status);

        if(status.GetStatus != StatusFuncReturn.StopAllSubscriberAndParent && status.GetStatus != StatusFuncReturn.StopParent)
        {
            if (victim.Player.PlayerProperties != null)
            {
                Player _victim = victim.Player.PlayerProperties;

                if (attacker.Player.PlayerProperties != null)
                {
                    Player _attacker = attacker.Player.PlayerProperties;

                    if(_victim.PlayerTeam != _attacker.PlayerTeam)
                    {
                        _victim.Health -= damage;
                    }
                }

                if (_victim.Health <= 0 && _victim.IsAlive)
                {
                    StartCoroutine(DeathPlayer(victim.Player));
                }
            }
        }

        RpcClientTakeDamage(victim, attacker, damage, status);
        EventTakeDamage_After?.Invoke(victim, attacker, damage, status);

        yield return null;
    }

    private IEnumerator SpawnPlayer(Entity.PlayerEnt Player, float Timer, Player.Team team)
    {
        yield return new WaitForSeconds(Timer);

        if (Player == null)
            yield break;

        StatusFunc status = new StatusFunc();
        Player GetPlayer = Player.PlayerProperties;

        EventSpawnPlayer_Before?.Invoke(Player, status);

        if (GetPlayer.PlayerTeam == global::Player.Team.Spectator)
            yield break;

        if(GetPlayer.Bot)
            GetPlayer.Bot.navMeshAgent.enabled = false;

        Player.Obj.transform.position = new Vector2(GameControl.Spawns[GetPlayer.RandomSpawn].position.x, GameControl.Spawns[GetPlayer.RandomSpawn].position.y);
        Player.Obj.transform.rotation = GameControl.Spawns[GetPlayer.RandomSpawn].rotation;

        GetPlayer.PlayerTeam = team;

        GetPlayer.IsAlive = true;
        GetPlayer.IsPainShock = false;
        GetPlayer.IsRegeneration = false;
        GetPlayer.Health = 1;
        GetPlayer.Armor = 0;
        GetPlayer.ActiveWeapon = 0;

        GetPlayer.Pain = null;
        GetPlayer.Regeneration = null;

        if (GetPlayer.Bot)
            GetPlayer.Bot.navMeshAgent.enabled = true;

        RpcClientSpawnPlayer(Player, status);
        EventSpawnPlayer_After?.Invoke(Player, status);

        yield return null;
    }

    private IEnumerator DeathPlayer(Entity.PlayerEnt Player)
    {
        if (Player == null)
            yield break;
        
        StatusFunc status = new StatusFunc();
        Player GetPlayer = Player.PlayerProperties;
        EventDeathPlayer_Before?.Invoke(Player, status);

        GetPlayer.IsAlive = false;
        GetPlayer.IsRegeneration = false;
        GetPlayer.IsPainShock = false;

        GetPlayer.Pain = null;
        GetPlayer.Regeneration = null;

        if (GetPlayer.Bot && isServer)
        {
            GetPlayer.Bot.BotStop();
            GetPlayer.Bot.navMeshAgent.enabled = false;
        }

        RpcClientDeathPlayer(Player, status);
        EventDeathPlayer_After?.Invoke(Player, status);

        yield return null;
    }

    //Client Events
    [ClientRpc]
    void RpcClientTakeDamage(Entity victim, Entity attacker, int damage, StatusFunc status)
    {
        if (isServerOnly)
            return;

        EventTakeDamage_Client?.Invoke(victim, attacker, damage, status);
    }

    [ClientRpc]
    void RpcClientSpawnPlayer(Entity.PlayerEnt Player, StatusFunc status)
    {
        if (isServerOnly)
            return;

        EventSpawnPlayer_Client?.Invoke(Player, status);
    }

    [ClientRpc]
    void RpcClientDeathPlayer(Entity.PlayerEnt Player, StatusFunc status)
    {
        if (isServerOnly)
            return;

        EventDeathPlayer_Client?.Invoke(Player, status);
    }
}
