﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieClassSystem : MonoBehaviour
{
    public string Name;
    public int Health;
    public float Speed;
    public float KnockBack;
    public int RegerationHP;
    public float RegerationSpeed;
    public float PainShock;
    public float PainShockSpeed;

    public WeaponSystem ZombieKnife;

    public AudioClip[] FeetClips;
    public AudioClip[] DeathClips;
    public AudioClip[] PainClips;
    public AudioClip[] InfectionClips;
}
