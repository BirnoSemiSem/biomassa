﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class WeaponPlayerControl : NetworkBehaviour
{
    // Start is called before the first frame update
    public Player PlayerControl;

    public Transform FirePoint;
    [SerializeField] private CapsuleCollider2D HitColiderKnifeAttack;
    [SerializeField] private HitKnife PlayerHitKnife;

    public GameObject Bullet;
    public GameObject MuzzleFlash;
    public AnimStartAndDestroyToTime AnimMuzzleFlash;

    public Coroutine CorReload;
    public Coroutine CorChangeWeapon;

    [SyncVar] public bool IsFire = false;
    [SyncVar] public bool IsReload = false;
    [SyncVar] public bool IsDraw = false;

    float FireCurrentTime;
    float FireNextTime;

    //Bot
    private BotControl Bot;

    //Delegate
    public UnityAction<Player, int> EventPlayerAttackFireWeapon;
    public UnityAction<Player, int, HitKnife> EventPlayerAttackFireKnife;
    public UnityAction<Player, int> EventPlayerStartReloading;
    public UnityAction<Player, int> EventPlayerEndReloading;
    public UnityAction<Player, int> EventPlayerStartSoundReloading;
    public UnityAction<Player, int> EventPlayerEndSoundReloading;
    public UnityAction<Player, int> EventPlayerStartDraw;
    public UnityAction<Player, int> EventPlayerEndDraw;

    void Start()
    {
        FireNextTime = FireCurrentTime = 0;

        if (GetComponent<BotControl>())
            Bot = GetComponent<BotControl>();
        else
            Bot = null;
    }

    void OnEnable()
    {
        GameObjStateControl.EventSpawnPlayer_After += ResetPlayerWeaponProperties;
        GameObjStateControl.EventDeathPlayer_After += ResetPlayerWeaponProperties;

        Biohazard.EventInfectionPlayer += InfectionPlayer;
    }

    void OnDisable()
    {
        GameObjStateControl.EventSpawnPlayer_After -= ResetPlayerWeaponProperties;
        GameObjStateControl.EventDeathPlayer_After -= ResetPlayerWeaponProperties;

        Biohazard.EventInfectionPlayer -= InfectionPlayer;
    }

    //Update is called once per frame
    void Update()
    {
        if (PlayerControl.IsAlive)
        {
            if (PlayerControl.PlayerTeam == global::Player.Team.Human)
            {
                if (!GameControl.GetIsFreeze)
                {
                    FireCurrentTime += Time.deltaTime;

                    if (isServer)
                    {
                        if (IsFire && PlayerControl.Weapons[PlayerControl.ActiveWeapon].Clip > 0 && FireCurrentTime >= FireNextTime && !IsReload && !IsDraw)
                        {
                            //БВЭЭЭЭЭ. Переделать Recoil
                            float Recoil = Random.Range(1, 10) > 5 ? (Random.Range(0f, PlayerControl.Weapons[PlayerControl.ActiveWeapon].MaxRecoil) * -1) : Random.Range(0f, PlayerControl.Weapons[PlayerControl.ActiveWeapon].MaxRecoil);
                            
                            int Clip = PlayerControl.Weapons[PlayerControl.ActiveWeapon].Clip - 1;
                            float NextTime = FireCurrentTime + PlayerControl.Weapons[PlayerControl.ActiveWeapon].FireSpeed;

                            Shoot(Recoil);
                            RpcShotFire(Recoil, Clip, NextTime);

                            transform.rotation = Quaternion.Euler(new Vector3(0, 0, transform.eulerAngles.z + Recoil));

                            if (!isServerOnly)
                            {
                                if (!MuzzleFlash.activeInHierarchy)
                                {
                                    MuzzleFlash.SetActive(true);
                                    AnimMuzzleFlash = MuzzleFlash.GetComponent<AnimStartAndDestroyToTime>();
                                    AnimMuzzleFlash.Go();
                                    MuzzleFlash.transform.position = FirePoint.position;
                                    MuzzleFlash.transform.rotation = Quaternion.Euler(new Vector3(0, 0, FirePoint.transform.eulerAngles.z + Recoil));
                                }
                            }

                            FireNextTime = FireCurrentTime + PlayerControl.Weapons[PlayerControl.ActiveWeapon].FireSpeed;
                            PlayerControl.Weapons[PlayerControl.ActiveWeapon].Clip--;

                            EventPlayerAttackFireWeapon?.Invoke(PlayerControl, PlayerControl.GetPlayerID);
                        }
                    }
                    //else
                    //{
                    //    if (IsFire && PlayerControl.Weapons[PlayerControl.ActiveWeapon].Clip > 0 && FireCurrentTime >= FireNextTime && !IsReload && !IsDraw)
                    //    {
                    //        float Recoil = Random.Range(1, 10) > 5 ? (Random.Range(0f, PlayerControl.Weapons[PlayerControl.ActiveWeapon].MaxRecoil) * -1) : Random.Range(0f, PlayerControl.Weapons[PlayerControl.ActiveWeapon].MaxRecoil);

                    //        transform.rotation = Quaternion.Euler(new Vector3(0, 0, transform.eulerAngles.z + Recoil));

                    //        GameObject _muzzleflash = Instantiate(MuzzleFlash, FirePoint.position, Quaternion.Euler(new Vector3(0, 0, FirePoint.transform.eulerAngles.z + Recoil)));

                    //        _muzzleflash.transform.SetParent(transform);

                    //        FireNextTime = FireCurrentTime + PlayerControl.Weapons[PlayerControl.ActiveWeapon].FireSpeed;
                    //        PlayerControl.Weapons[PlayerControl.ActiveWeapon].Clip--;

                    //        EventPlayerAttackFireWeapon?.Invoke(gameObject, PlayerControl.GetPlayerID);
                    //    }
                    //}
                }

                if (isServer && !IsReload && !IsDraw && PlayerControl.Weapons[PlayerControl.ActiveWeapon].Clip <= 0 && PlayerControl.Weapons[PlayerControl.ActiveWeapon].Ammo > 0)
                {
                    RpcStartReload();
                    CorReload = StartCoroutine(Reload());
                }
            }
            else if (PlayerControl.PlayerTeam == global::Player.Team.Zombie)
            {
                if (IsFire && PlayerControl.Weapons[PlayerControl.ActiveWeapon].Weapon == WeaponSystem.TypeWeapon.Knife && !PlayerControl.Animator.GetBool("IsKnifeAttack") && !HitColiderKnifeAttack.enabled)
                {
                    PlayerControl.Animator.SetBool("IsKnifeAttack", true);
                }
                else if(!IsFire && PlayerControl.Animator.GetBool("IsKnifeAttack"))
                {
                    PlayerControl.Animator.SetBool("IsKnifeAttack", false);
                }
            }
        }
    }

    public void ClickButtonFireEnter()
    {
        if (!isServer && Bot)
            return;

        if (isServer)
        {
            //RpcStartFire();
            IsFire = true;
        }
        else
        {
            CmdFireEnter();
            IsFire = true;
        }
    }

    public void ClickButtonFireExit()
    {
        if (!isServer && Bot)
            return;

        if (isServer)
        {
            //RpcEndFire();
            IsFire = false;
        }
        else
        {
            CmdFireExit();
            IsFire = false;
        }
    }

    public void ClickButtonReloadEnter()
    {
        if (!isServer && Bot)
            return;

        if (!IsReload && !IsDraw && PlayerControl.Weapons[PlayerControl.ActiveWeapon].Clip < PlayerControl.Weapons[PlayerControl.ActiveWeapon].MaxClip && PlayerControl.Weapons[PlayerControl.ActiveWeapon].Ammo > 0)
        {
            if (isServer)
            {
                RpcStartReload();
                CorReload = StartCoroutine(Reload());
            }
            else
            {
                CmdStartReload();
                CorReload = StartCoroutine(Reload());
            }
        }
    }

    public void ClickChangeWeapon(int WeaponSelect)
    {
        if (PlayerControl.PlayerTeam != global::Player.Team.Human)
            return;

        if (WeaponSelect == PlayerControl.ActiveWeapon)
            return;

        if (isServer)
        {
            IsDraw = false;

            RpcStartChangeWeapon(WeaponSelect);
            PlayerControl.ActiveWeapon = WeaponSelect;

            if (CorChangeWeapon != null)
                StopCoroutine(CorChangeWeapon);

            CorChangeWeapon = StartCoroutine(ChangeWeapon());
        }
        else
        {
            IsDraw = false;

            CmdStartChangeWeapon(WeaponSelect);
            PlayerControl.ActiveWeapon = WeaponSelect;

            if (CorChangeWeapon != null)
                StopCoroutine(CorChangeWeapon);

            CorChangeWeapon = StartCoroutine(ChangeWeapon());
        }
    }

    public void KnifeAttack()
    {
        if (PlayerControl.IsAlive)
        {
            HitColiderKnifeAttack.enabled = !HitColiderKnifeAttack.enabled;
            HitColiderKnifeAttack.isTrigger = !HitColiderKnifeAttack.isTrigger;

            if (!HitColiderKnifeAttack.enabled)
            {
                PlayerControl.Animator.SetBool("IsKnifeAttack", HitColiderKnifeAttack.enabled);
            }
            else
            {
                EventPlayerAttackFireKnife?.Invoke(PlayerControl, PlayerControl.GetPlayerID, PlayerHitKnife);
            }
        }
    }

    void Shoot(float Recoil)
    {
        GameObject _bullet = OptimizationLogic.OptCode.GetPoolObject(OptimizationLogic.Effects.Bullet);
        _bullet.transform.position = FirePoint.position;
        _bullet.transform.rotation = Quaternion.Euler(new Vector3(0, 0, FirePoint.transform.eulerAngles.z + Recoil));
        _bullet.SetActive(true);
        _bullet.GetComponent<Bullet>().Push(gameObject);
    }

    void InfectionPlayer(Player Player, int id)
    {
        ResetPlayerWeaponProperties(new Entity(id, Player.gameObject, Player).Player);
    }

    void ResetPlayerWeaponProperties(Entity.PlayerEnt Player, StatusFunc status = null)
    {
        if (Player.id == PlayerControl.GetPlayerID)
        {
            if(CorReload != null)
                StopCoroutine(CorReload);

            if(CorChangeWeapon != null)
                StopCoroutine(CorChangeWeapon);

            if(AnimMuzzleFlash)
                AnimMuzzleFlash.CancelAnim();

            PlayerControl.Weapons[PlayerControl.ActiveWeapon].Ammo = PlayerControl.Weapons[PlayerControl.ActiveWeapon].MaxAmmo;
            PlayerControl.Weapons[PlayerControl.ActiveWeapon].Clip = PlayerControl.Weapons[PlayerControl.ActiveWeapon].MaxClip;

            FireNextTime = FireCurrentTime = 0;

            IsReload = false;
            IsFire = false;
            IsDraw = false;

            HitColiderKnifeAttack.enabled = false;
            HitColiderKnifeAttack.isTrigger = false;
        }
    }

    void SwicthWeapon()
    {
        if (!PlayerControl.IsAlive)
            return;

        PlayerControl.FireClip = PlayerControl.Weapons[PlayerControl.ActiveWeapon].FireClip;
        PlayerControl.DrawClip = PlayerControl.Weapons[PlayerControl.ActiveWeapon].DrawClip;
        PlayerControl.ReloadClips = PlayerControl.Weapons[PlayerControl.ActiveWeapon].ReloadClips;

        PlayerControl.KnifeMissClips = PlayerControl.Weapons[PlayerControl.ActiveWeapon].KnifeMissClips;
        PlayerControl.KnifeStrikeClips = PlayerControl.Weapons[PlayerControl.ActiveWeapon].KnifeStrikeClips;

        PlayerControl.WeaponTransform.localPosition = PlayerControl.Weapons[PlayerControl.ActiveWeapon].PositionSpr;
        PlayerControl.WeaponSprite.sprite = PlayerControl.Weapons[PlayerControl.ActiveWeapon].WeaponSpr;

        if (PlayerControl.PlayerTeam == global::Player.Team.Human)
        {
            Player.AnimationPlayer animationPlayer = (Player.AnimationPlayer)PlayerControl.ActiveWeapon + 1;
            AnimationObj.AnimationPlayer(PlayerControl, PlayerControl.HumanClass.Name + "-" + animationPlayer.ToString());
        }
    }

    public IEnumerator Reload()
    {
        IsReload = true;

        EventPlayerStartReloading?.Invoke(PlayerControl, PlayerControl.GetPlayerID);

        yield return new WaitForSeconds(0.2f);
        EventPlayerStartSoundReloading?.Invoke(PlayerControl, PlayerControl.GetPlayerID);
        yield return new WaitForSeconds(PlayerControl.Weapons[PlayerControl.ActiveWeapon].ReloadTime - 0.5f);
        EventPlayerEndSoundReloading?.Invoke(PlayerControl, PlayerControl.GetPlayerID);
        yield return new WaitForSeconds(0.3f);

        if (PlayerControl.Weapons[PlayerControl.ActiveWeapon].Ammo >= PlayerControl.Weapons[PlayerControl.ActiveWeapon].MaxClip)
        {
            if (!GameControl.GetIsInfinityAmmo)
                PlayerControl.Weapons[PlayerControl.ActiveWeapon].Ammo -= PlayerControl.Weapons[PlayerControl.ActiveWeapon].MaxClip - PlayerControl.Weapons[PlayerControl.ActiveWeapon].Clip;
            PlayerControl.Weapons[PlayerControl.ActiveWeapon].Clip = PlayerControl.Weapons[PlayerControl.ActiveWeapon].MaxClip;
        }
        else
        {
            PlayerControl.Weapons[PlayerControl.ActiveWeapon].Clip = PlayerControl.Weapons[PlayerControl.ActiveWeapon].Ammo;
            if (!GameControl.GetIsInfinityAmmo)
                PlayerControl.Weapons[PlayerControl.ActiveWeapon].Ammo = 0;
        }

        IsReload = false;
        EventPlayerEndReloading?.Invoke(PlayerControl, PlayerControl.GetPlayerID);
    }

    public IEnumerator ChangeWeapon()
    {
        if (IsReload)
        {
            if(CorReload != null)
                StopCoroutine(CorReload);
            IsReload = false;
        }

        IsDraw = true;

        SwicthWeapon();

        EventPlayerStartDraw?.Invoke(PlayerControl, PlayerControl.GetPlayerID);

        yield return new WaitForSeconds(1f);

        IsDraw = false;
        EventPlayerEndDraw?.Invoke(PlayerControl, PlayerControl.GetPlayerID);
    }

    #region [ClientRpc]
    //[ClientRpc]
    //void RpcStartFire()
    //{
    //    if (isServer)
    //        return;

    //    IsFire = true;
    //}

    //[ClientRpc]
    //void RpcEndFire()
    //{
    //    if (isServer)
    //        return;

    //    IsFire = false;
    //}

    [ClientRpc]
    void RpcStartReload()
    {
        if (isServer)
            return;

        CorReload = StartCoroutine(Reload());
    }

    [ClientRpc]
    void RpcStartChangeWeapon(int WeaponSelect)
    {
        if (isServer)
            return;

        PlayerControl.ActiveWeapon = WeaponSelect;

        if (CorChangeWeapon != null)
            StopCoroutine(CorChangeWeapon);

        CorChangeWeapon = StartCoroutine(ChangeWeapon());
    }

    [ClientRpc]
    void RpcShotFire(float Recoil, int Clip, float Time)
    {
        if (isServer)
            return;

        PlayerControl.transform.rotation = Quaternion.Euler(new Vector3(0, 0, PlayerControl.transform.eulerAngles.z + Recoil));

        if (!MuzzleFlash.activeInHierarchy)
        {
            MuzzleFlash.SetActive(true);

            AnimMuzzleFlash = MuzzleFlash.GetComponent<AnimStartAndDestroyToTime>();
            AnimMuzzleFlash.Go();

            MuzzleFlash.transform.position = FirePoint.position;
            MuzzleFlash.transform.rotation = Quaternion.Euler(new Vector3(0, 0, FirePoint.transform.eulerAngles.z + Recoil));
        }

        FireNextTime = Time;
        PlayerControl.Weapons[PlayerControl.ActiveWeapon].Clip = Clip;

        Shoot(Recoil);

        EventPlayerAttackFireWeapon?.Invoke(PlayerControl, PlayerControl.GetPlayerID);
    }

    //[ClientRpc]
    //void RpcStartKnifeAttack()
    //{
    //    if (isServer)
    //        return;

    //    PlayerControl.Animator.SetBool("IsKnifeAttack", true);
    //}

    //[ClientRpc]
    //void RpcEndKnifeAttack()
    //{
    //    if (isServer)
    //        return;

    //    PlayerControl.Animator.SetBool("IsKnifeAttack", false);
    //}

    //[ClientRpc]
    //void RpcKnifeAttack()
    //{
    //    if (isServer)
    //        return;

    //    if (PlayerControl.IsAlive)
    //    {
    //        HitColiderKnifeAttack.enabled = !HitColiderKnifeAttack.enabled;
    //        HitColiderKnifeAttack.isTrigger = !HitColiderKnifeAttack.isTrigger;

    //        if (!HitColiderKnifeAttack.enabled)
    //        {
    //            PlayerControl.Animator.SetBool("IsKnifeAttack", HitColiderKnifeAttack.enabled);
    //        }
    //        else
    //        {
    //            EventPlayerAttackFireKnife?.Invoke(Player, Player.GetInstanceID(), PlayerHitKnife);
    //        }
    //    }
    //}
    #endregion

    #region [Command]
    [Command]
    void CmdReset(GameObject Player, int id)
    {
        if (!isServer)
            return;

        ResetPlayerWeaponProperties(new Entity(id, Player, Player.GetComponent<Player>()).Player);
    }

    [Command]
    void CmdFireEnter()
    {
        if (!isServer)
            return;

        IsFire = true;
    }

    [Command]
    void CmdFireExit()
    {
        if (!isServer)
            return;

        IsFire = false;
    }

    [Command]
    void CmdStartReload()
    {
        if (!isServer)
            return;

        CorReload = StartCoroutine(Reload());
    }

    [Command]
    void CmdStartChangeWeapon(int WeaponSelect)
    {
        if (!isServer)
            return;

        if (CorChangeWeapon != null)
            StopCoroutine(CorChangeWeapon);

        PlayerControl.ActiveWeapon = WeaponSelect;
        CorChangeWeapon = StartCoroutine(ChangeWeapon());
    }

    [Command]
    void CmdShotFire(float Recoil)
    {
        if (!isServer)
            return;

        transform.rotation = Quaternion.Euler(new Vector3(0, 0, transform.eulerAngles.z + Recoil));

        FireNextTime = FireCurrentTime + PlayerControl.Weapons[PlayerControl.ActiveWeapon].FireSpeed;
        PlayerControl.Weapons[PlayerControl.ActiveWeapon].Clip--;

        EventPlayerAttackFireWeapon?.Invoke(PlayerControl, PlayerControl.GetPlayerID);
    }

    [Command]
    void CmdStartKnifeAttack()
    {
        if (!isServer)
            return;

        PlayerControl.Animator.SetBool("IsKnifeAttack", true);
    }

    [Command]
    void CmdEndKnifeAttack()
    {
        if (!isServer)
            return;

        PlayerControl.Animator.SetBool("IsKnifeAttack", false);
    }

    [Command]
    void CmdKnifeAttack()
    {
        if (!isServer)
            return;

        if (PlayerControl.IsAlive)
        {
            HitColiderKnifeAttack.GetComponent<CapsuleCollider2D>().enabled = !HitColiderKnifeAttack.GetComponent<CapsuleCollider2D>().enabled;
            HitColiderKnifeAttack.GetComponent<CapsuleCollider2D>().isTrigger = !HitColiderKnifeAttack.GetComponent<CapsuleCollider2D>().isTrigger;

            if (!HitColiderKnifeAttack.GetComponent<CapsuleCollider2D>().enabled)
            {
                PlayerControl.Animator.SetBool("IsKnifeAttack", !HitColiderKnifeAttack.GetComponent<CapsuleCollider2D>().enabled);
            }
            else
            {
                EventPlayerAttackFireKnife?.Invoke(PlayerControl, PlayerControl.GetPlayerID, HitColiderKnifeAttack.GetComponent<HitKnife>());
            }
        }
    }

    //[Command]
    //void CmdShot(float Recoil)
    //{
    //    if (!isServer)
    //        return;

    //    GameObject _bulet = Instantiate(Bullet, FirePoint.position, Quaternion.Euler(new Vector3(0, 0, FirePoint.transform.eulerAngles.z + Recoil)));
    //    GameObject _muzzleflash = Instantiate(MuzzleFlash, FirePoint.position, Quaternion.Euler(new Vector3(0, 0, FirePoint.transform.eulerAngles.z + Recoil)));

    //    NetworkServer.Spawn(_bulet);
    //    NetworkServer.Spawn(_muzzleflash);

    //    _bulet.transform.SetParent(transform);
    //    _muzzleflash.transform.SetParent(transform);

    //    _bulet.GetComponent<Bullet>().Go();
    //}
    #endregion
}
