using UnityEngine;
using UnityEngine.AI;

public class PathCalculate : MonoBehaviour
{
    static public Vector3 RandomBoxPoint(Vector3 Point, float TempX, float TempY)
    {
        float RandomX = 0f;
        float RandomY = 0f;

        // ���������� �������
        double CountTry = (TempX * TempY) * 100;

        Vector3 temp = Vector3.zero;

        while (CountTry > 0)
        {
            // ��������� ��������� ��������
            RandomX = Random.Range(-TempX, TempX);
            RandomY = Random.Range(-TempY, TempY);

            // ��������� ����� �����
            temp = new Vector3(Point.x + RandomX, Point.y + RandomY);

            // �������� ����� �� ���������� � �������������� NavMesh.SamplePosition
            NavMeshHit hit;
            if (NavMesh.SamplePosition(temp, out hit, 1.0f, NavMesh.AllAreas))
            {
                // ���� ����� ������� �� NavMesh, �� ����� ����� �� �����
                temp = hit.position;
                break;
            }

            CountTry--;
        }

        // ���� ������� ���������, ���������� 0,0,0
        if (CountTry <= 0)
        {
            temp = Vector3.zero;
        }

        return temp;
    }

    static public float CalculatePathDistance(NavMeshPath path)
    {
        float totalDistance = 0f;

        for (int i = 0; i < path.corners.Length - 1; i++)
        {
            totalDistance += Vector3.Distance(path.corners[i], path.corners[i + 1]);
        }

        return totalDistance;
    }

    static public Vector3 CalculateBezierPoint(float t, Vector3 p0, Vector3 p1, Vector3 p2)
    {
        float u = 1 - t;
        float tt = t * t;
        float uu = u * u;

        Vector3 p = uu * p0; // (1 - t)^2 * P0
        p += 2 * u * t * p1; // 2 * (1 - t) * t * P1
        p += tt * p2; // t^2 * P2

        return p;
    }
}
