﻿using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BotControl : NetworkBehaviour
{
    // Start is called before the first frame update

    public enum BotStatus
    {
        IsRunning,
        IsRunningAndIsAttack,
        IsRunningShelter,
        IsRunningShelterAndIsAttack,
        IsFollowPlayer,
        IsFollowPlayerAndIsAttack,
        IsPatrolling,
        IsStoping
    };

    public enum LookingType
    {
        AtPlayer,
        AtBunker,
        AtRandomAngle,
        None
    };

    private float AngleLooking;

    public BotStatus Bot;
    public LookingType BotLook;

    public bool IsBotWalk = false;
    public bool IsBotFollowPlayer = false;
    public bool IsBotRunToShelterPositionForBunker = false;
    public bool IsBotInBunker = false;
    public bool IsBotInDanger = false;
    public bool IsBotInDangerEscape = false;

    public bool IsBotHunt = false;
    public bool IsBotFollow = false;

    public NavMeshAgent navMeshAgent;
    [SerializeField] private Vector2 TargetPointPath;
    private List<Vector2> BotPath;
    [SerializeField] private int MaxNumPointsOnBezier = 15;
    [SerializeField] private int MaxKoffDistForMindPoint = 5;

    [SyncVar] public Vector2 VelocityMove;
    public Player TargetAim;

    public Rigidbody2D rb;
    [SerializeField] private Animator FeetAnim;
    [SerializeField] private SpriteRenderer FeetSpr;

    public Player PlayerControl;
    public Transform FirePointTransform;

    private Coroutine CorBotDanger;

    // [Header("Options Bot")]

    #region [Options for Bot Status]
    public int SuccessRateOtherStatus {get; private set; } = 25;
    public int SuccessRateLogicGroupActivated {get; private set; } = 30;

    // [Space]

    public float MinTimerChangeBotStatusBeforeStartEvent {get; private set; } = 0.1f;
    public float MaxTimerChangeBotStatusBeforeStartEvent {get; private set; } = 15f;
    public float MinTimerChangeBotStatusAfterStartEvent {get; private set; } = 15f;
    public float MaxTimerChangeBotStatusAfterStartEvent {get; private set; } = 90f;
    #endregion

    // [Space]

    #region [Options Detect/Attack]
    public float MinTimerDelayDetect {get; private set; } = 0.5f;
    public float MaxTimerDelayDetect {get; private set; } = 1.0f;
    public float RadiusPlayerDetect {get; private set; } = 2f;
    public float RadiusKnifeAttack {get; private set; } = 0.3f;
    #endregion

    // [Space]

    #region [Options Weapon actions]
    public int SuccessRateChangeWeapon {get; private set; } = 50;

    [Space]

    private float CurrentLeftTimeChangeWeapon = 0;
    public float MinTimerChangeWeapon {get; private set; } = 5f;
    public float MaxTimerChangeWeapon {get; private set; } = 11f;

    [Space]

    private float CurrentLeftTimeWeaponForReload = 0;
    public float MinTimerWeaponForReload {get; private set; } = 3f;
    public float MaxTimerWeaponForReload {get; private set; } = 6f;
    #endregion

    [Space]

    #region [Options Look]
    private float CurrentLeftTimeLookStatus = 0;
    public float MinTimerLookStatus {get; private set; } = 5f;
    public float MaxTimerLookStatus {get; private set; } = 16f;

    // [Space]

    public float LookSpeedNormal {get; private set; } = 2f;
    public float LookSpeedDangerHuman {get; private set; } = 10f;
    public float LookSpeedDangerZombie {get; private set; } = 5f;
    #endregion

    [Space]

    [SerializeField] private WeaponPlayerControl WeaponControl;
    [SerializeField] private GameObject FirePoint;

    private bool IsVisible;
    [SerializeField] private bool IsOptimization;

    public bool IsDebug = false;

    void Start()
    {
        if (navMeshAgent == null)
            navMeshAgent = GetComponent<NavMeshAgent>();

        navMeshAgent.avoidancePriority = UnityEngine.Random.Range(1, 101);

        if (rb == null)
            rb = GetComponent<Rigidbody2D>();

        if (WeaponControl == null)
            WeaponControl = GetComponent<WeaponPlayerControl>();

        if (isServer)
        {
            Bot = BotStatus.IsStoping;
            BotLook = LookingType.None;
            LookTarget(Vector3.zero, LookSpeedNormal);
        }

        CurrentLeftTimeWeaponForReload = UnityEngine.Random.Range(MinTimerWeaponForReload, MaxTimerWeaponForReload);
        CurrentLeftTimeChangeWeapon = UnityEngine.Random.Range(MinTimerChangeWeapon, MaxTimerChangeWeapon);
        CurrentLeftTimeLookStatus = UnityEngine.Random.Range(MinTimerLookStatus, MaxTimerLookStatus);

        CorBotDanger = null;

        navMeshAgent.updateRotation = false;
        navMeshAgent.updateUpAxis = false;

    }

    void OnEnable()
    {
        PhysicalEntity.SetEntityInList(PlayerControl.GetPlayerID, gameObject, EntityIndex.Bot);

        GameControl.EventPreStartRound += NewRound;
        GameControl.EventRoundStart += StartRound;
        GameControl.EventRoundEnd += EndRound;

        GameObjStateControl.EventSpawnPlayer_After += Spawn;
        
        Biohazard.EventBiohazardStart += GameStart;
    }

    void OnDisable()
    {
        PhysicalEntity.RemoveEntityInList(PlayerControl.GetPlayerID, gameObject, EntityIndex.Bot);

        GameControl.EventPreStartRound -= NewRound;
        GameControl.EventRoundStart -= StartRound;
        GameControl.EventRoundEnd -= EndRound;

        GameObjStateControl.EventSpawnPlayer_After -= Spawn;

        Biohazard.EventBiohazardStart -= GameStart;
    }

    public void NewRound()
    {
        if (!isServer)
            return;

        if (!IsDebug)
        {
            Bot = BotStatus.IsStoping;
            BotStop();
        }
    }

    public void Spawn(Entity.PlayerEnt Player, StatusFunc status)
    {
        if (Player.id != PlayerControl.GetPlayerID)
            return;

        if(GameControl.GetIsGame)
        {
            if(Biohazard.IsModBiohazardStart)
            {
                if (PlayerControl.PlayerTeam == global::Player.Team.Zombie)
                    Bot = BotStatus.IsRunning;
            }
        }

        CurrentLeftTimeWeaponForReload = UnityEngine.Random.Range(MinTimerWeaponForReload, MaxTimerWeaponForReload);
        CurrentLeftTimeChangeWeapon = UnityEngine.Random.Range(MinTimerChangeWeapon, MaxTimerChangeWeapon);
        CurrentLeftTimeLookStatus = UnityEngine.Random.Range(MinTimerLookStatus, MaxTimerLookStatus);

        if (CorBotDanger != null)
        {
            StopCoroutine(CorBotDanger);
            CorBotDanger = null;
        }
    }

    public void StartRound()
    {
        if (!isServer)
            return;

        if (!IsDebug)
        {
            if (PlayerControl.IsAlive)
            {
                if (UnityEngine.Random.Range(1, 101) <= SuccessRateOtherStatus)
                {
                    Bot = BotStatus.IsRunningShelter;
                }
                else
                {
                    Bot = BotStatus.IsRunning;
                    Invoke(nameof(ChangeBotStatus), UnityEngine.Random.Range(MinTimerChangeBotStatusBeforeStartEvent, MaxTimerChangeBotStatusBeforeStartEvent));
                }
            }
        }
    }

    public void EndRound()
    {
        if (!isServer)
            return;

        if (!IsDebug)
        {
            BotStop();
            Bot = BotStatus.IsRunning;
        }
    }

    public void GameStart()
    {
        if (!isServer)
            return;

        if (PlayerControl.IsAlive && Bot != BotStatus.IsStoping)
        {
            if (UnityEngine.Random.Range(1, 101) <= SuccessRateOtherStatus)
                Invoke(nameof(ChangeBotStatus), UnityEngine.Random.Range(MinTimerChangeBotStatusBeforeStartEvent, MaxTimerChangeBotStatusBeforeStartEvent));
            else
                Bot = BotStatus.IsRunningShelter;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!PlayerControl.IsAlive)
            return;

        switch (PlayerControl.PlayerTeam)
        {
            case Player.Team.Zombie: { navMeshAgent.speed = PlayerControl.ZombieClass.Speed * (PlayerControl.IsPainShock ? PlayerControl.ZombieClass.PainShock : 1f); break; }
            case Player.Team.Human: { navMeshAgent.speed = PlayerControl.HumanClass.Speed * (PlayerControl.IsPainShock ? PlayerControl.HumanClass.PainShock : 1f); break; }
        }

        if (isServer)
        {
            VelocityMove = navMeshAgent.velocity;

            BotStateLogic();
            BotWeaponsStateLogic();
            BotWalk();

            if (CorBotDanger == null)
                CorBotDanger = StartCoroutine(BotDanger());

            if (TargetAim &&
                PlayerControl.PlayerTeam != TargetAim.PlayerTeam &&
                TargetAim.IsAlive)
            {
                switch (PlayerControl.PlayerTeam)
                {
                    case Player.Team.Zombie:
                    {
                        LookTarget(TargetAim.transform.position, LookSpeedDangerZombie);
                        break;
                    }
                    case Player.Team.Human:
                    {
                        LookTarget(TargetAim.transform.position, LookSpeedDangerHuman);
                        break;
                    }
                }
            }
            else
                TargetAim = null;
        }

        //AnimWalk();

        AnimationObj.AnimationPlayerFeet(PlayerControl.Feet,
            GameControl.GetIsFreeze ? 0f : VelocityMove.magnitude / 2f
            , VelocityMove);

        if(VelocityMove != Vector2.zero)
        {
            if (Bot == BotStatus.IsRunning || Bot == BotStatus.IsFollowPlayer || Bot == BotStatus.IsPatrolling || Bot == BotStatus.IsRunningShelter)
            {
                if (BotLook == LookingType.None)
                    LookTarget((Vector3)VelocityMove + transform.position, LookSpeedNormal);
            }
        }

        if (IsDebug)
            Debug.DrawLine(transform.position, FirePointTransform.position, Color.green);
    }

    //private void OnDrawGizmos()
    //{
    //    Gizmos.color = Color.red;
    //    Gizmos.DrawLine(transform.position, FirePointTransform.position);
    //}

    private void BotWalk()
    {
        // Проверяем, есть ли путь и отрисовываем его
        if (BotPath != null && BotPath.Count > 1)
        {
            if (IsDebug)
            {
                for (int i = 0; i < BotPath.Count - 1; i++)
                {
                    Debug.DrawLine(BotPath[i], BotPath[i + 1], Color.green);
                }
            }

            // Перемещаем агента по маршруту
            if (navMeshAgent.remainingDistance < 0.1f && BotPath.Count > 1)
            {
                navMeshAgent.SetDestination(BotPath[1]);
                BotPath.RemoveAt(0); // Удаляем пройденную точку
            }
        }
    }

    public void LookTarget(Vector3 TargetPosition, float speed)
    {
        if (!isServer)
            return;

        if (Bot == BotStatus.IsStoping)
            return;

        if (TargetPosition != Vector3.zero || BotLook == LookingType.AtRandomAngle)
        {
            if (BotLook != LookingType.AtRandomAngle || IsBotInDanger)
            {
                Vector2 Look = TargetPosition - transform.position;
                AngleLooking = Mathf.Atan2(Look.x, Look.y) * Mathf.Rad2Deg;
                AngleLooking = -AngleLooking;
                //Debug.Log("Test AngleLooking: " + AngleLooking);
            }
            else if (Mathf.RoundToInt(transform.eulerAngles.z) == Mathf.RoundToInt(AngleLooking))
                AngleLooking = UnityEngine.Random.Range(0f, 360.0f);

            if (Mathf.RoundToInt(transform.eulerAngles.z) != Mathf.RoundToInt(AngleLooking))
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(0, 0, AngleLooking)), speed * Time.deltaTime);
        }

        if (CurrentLeftTimeLookStatus >= 0)
            CurrentLeftTimeLookStatus -= Time.deltaTime;

        if (CurrentLeftTimeLookStatus < 0)
        {
            if (!IsBotInDanger)
                BotLook = (LookingType)UnityEngine.Random.Range(0, Enum.GetNames(typeof(LookingType)).Length);

            CurrentLeftTimeLookStatus = UnityEngine.Random.Range(MinTimerLookStatus, MaxTimerLookStatus);

            if (BotLook == LookingType.AtRandomAngle)
                AngleLooking = UnityEngine.Random.Range(0f, 360.0f);
        }
    }

    public void ChangeBotStatus()
    {
        if (!isServer)
            return;

        if (!Biohazard.IsModBiohazardStart)
            return;

        if (PlayerControl.IsAlive && Bot != BotStatus.IsStoping)
        {
            switch(PlayerControl.PlayerTeam)
            {
                case Player.Team.Zombie:
                {
/*                    switch(UnityEngine.Random.Range(0,2))
                    {
                        case 0:
                        {
                            Bot = BotStatus.IsRunning;
                            break;
                        }
                        case 1:
                        {
                            if(gameObject != Biohazard.BotLeaderZombie)
                                Bot = BotStatus.IsFollowPlayer;
                            else
                                Bot = BotStatus.IsRunning;

                            if (!Biohazard.IsBotGroupZombie && UnityEngine.Random.Range(1, 101) < SuccessRateLogicGroupActivated)
                                Biohazard.IsBotGroupZombie = true;
                            break;
                        }
                    }*/
                    Bot = BotStatus.IsRunning;
                    break;
                }
                case Player.Team.Human:
                {
                    switch (UnityEngine.Random.Range(0, 4))
                    {
                        case 0:
                        {
                            Bot = BotStatus.IsRunningShelter;
                            break;
                        }
                        case 1:
                        {
/*                            if (Biohazard.IsModBiohazardStart)
                            {
                                if (gameObject != Biohazard.BotLeaderHuman)
                                    Bot = BotStatus.IsFollowPlayer;
                                else
                                    Bot = BotStatus.IsRunningShelter;

                                if (!Biohazard.IsBotGroupHuman && UnityEngine.Random.Range(1, 101) < SuccessRateLogicGroupActivated)
                                    Biohazard.IsBotGroupHuman = true;
                            }
                            else*/
                                Bot = BotStatus.IsRunningShelter;
                            break;
                        }
                        case 2:
                        {
                            Bot = BotStatus.IsPatrolling;
                            break;
                        }
                        case 3:
                        {
                            if(!Biohazard.IsModBiohazardStart)
                                Bot = BotStatus.IsRunning;
                            else
                                Bot = BotStatus.IsRunningShelter;
                            break;
                        }
                    }
                    break;
                }
            }
        }
    }

    public void BotSetPointToRun(Vector2 Target)
    {
        if (!navMeshAgent.isOnNavMesh)
            return;

        if(PlayerControl.IsAlive && Target != TargetPointPath)
        {
            //navMeshAgent.SetDestination(Target);
            TargetPointPath = Target;

            if(TargetAim != null)
                navMeshAgent.SetDestination(Target);
            else
                CalculateBezierPath(TargetPointPath);
        }
    }

    // Функция для вычисления точек кривой Безье
    private void CalculateBezierPath(Vector2 Point)
    {
        BotPath = new List<Vector2>();
        NavMeshPath path = new NavMeshPath();

        NavMesh.CalculatePath(transform.position, Point, NavMesh.AllAreas, path);

        float totalLength = 0f;
        for (int i = 1; i < path.corners.Length; i++)
        {
            totalLength += Vector2.Distance(path.corners[i - 1], path.corners[i]);
        }

        Vector3[] pathCorners = path.corners;

        // Проходим по всем сегментам пути
        for (int i = 0; i < pathCorners.Length - 1; i++)
        {
            Vector2 startPoint = Vector2.zero;

            if (i > 0 && BotPath.Count > 0)
                startPoint = BotPath[BotPath.Count - 1];
            else
                startPoint = pathCorners[i];


            Vector2 endPoint = PathCalculate.RandomBoxPoint(pathCorners[i + 1], 0.25f, 0.25f);

            float distance = Vector2.Distance(startPoint, endPoint);

            if (distance < 0.25f)
                continue;

            if (distance / totalLength * 100 < 25)
            {
                BotPath.Add(endPoint);
                continue;
            }

            Vector2 ControlPointForBezier = Vector2.zero;
            NavMeshPath temppath = new NavMeshPath();
            int CountBuildBezierPath = 0;

            controlPoint:
            if (CountBuildBezierPath >= 3)
            {
                BotPath.Add(endPoint);
                continue;
            }

            for (int k = 0; k < distance / totalLength * MaxKoffDistForMindPoint; k++)
            {
                // Генерируем контрольную точку
                ControlPointForBezier = PathCalculate.RandomBoxPoint((startPoint + endPoint) / 2,
                    distance / totalLength * UnityEngine.Random.Range(1, MaxKoffDistForMindPoint),
                    distance / totalLength * UnityEngine.Random.Range(1, MaxKoffDistForMindPoint));
                

                if (ControlPointForBezier == Vector2.zero)
                    continue;

                NavMesh.CalculatePath(startPoint, ControlPointForBezier, NavMesh.AllAreas, temppath);

                if (temppath.corners.Length > 3)
                    continue;
                else
                    break;
            }

            if (ControlPointForBezier == Vector2.zero)
            {
                BotPath.Add(endPoint);
                continue;
            }

            List<Vector2> TempbezierPath = new List<Vector2>();

            for (int k = 0; k < MaxNumPointsOnBezier; k++)
            {
                int pointsPerSegment = UnityEngine.Random.Range(2, MaxNumPointsOnBezier);

                // Генерируем точки кривой Безье для сегмента
                for (float t = 0; t <= 1; t += 1f / pointsPerSegment)
                {
                    Vector2 bezierPoint = PathCalculate.CalculateBezierPoint(t, startPoint, ControlPointForBezier, endPoint);

                    NavMeshHit hit;
                    if (NavMesh.SamplePosition(bezierPoint, out hit, 1.0f, NavMesh.AllAreas))
                        TempbezierPath.Add(bezierPoint);
                    else
                    {
                        CountBuildBezierPath++;
                        goto controlPoint;
                    }
                }

                if (TempbezierPath.Count > 0)
                {
                    BotPath.AddRange(TempbezierPath);
                    break;
                }
            }
        }
    }

    public void BotWalkStop()
    {
        if (!navMeshAgent.isOnNavMesh)
            return;

        if (PlayerControl.IsAlive)
        {
            TargetPointPath = Vector2.zero;
            BotPath = null;
            navMeshAgent.ResetPath();
        }
    }

    public void BotStop()
    {
        if (!navMeshAgent.isOnNavMesh)
            return;

        if (PlayerControl.IsAlive)
        {
            IsBotWalk = false;
            IsBotInBunker = false;
            IsBotFollowPlayer = false;
            IsBotFollow = false;
            IsBotRunToShelterPositionForBunker = false;
            IsBotInDanger = false;
            IsBotInDangerEscape = false;

            TargetAim = null;

            CurrentLeftTimeWeaponForReload = UnityEngine.Random.Range(MinTimerWeaponForReload, MaxTimerWeaponForReload);
            CurrentLeftTimeChangeWeapon = UnityEngine.Random.Range(MinTimerChangeWeapon, MaxTimerChangeWeapon);
            CurrentLeftTimeLookStatus = UnityEngine.Random.Range(MinTimerLookStatus, MaxTimerLookStatus);

            if (CorBotDanger != null)
            {
                StopCoroutine(CorBotDanger);
                CorBotDanger = null;
            }

            BotWalkStop();
            BotLook = LookingType.None;

            navMeshAgent.isStopped = false;
            TargetPointPath = Vector2.zero;
            BotPath = null;
        }
    }

    private void BotStateLogic()
    {
        if (!Biohazard.IsBotHuntForZombies || !Biohazard.IsBotHuntForHumans)
            if (!IsInvoking(nameof(ChangeBotStatus)))
                Invoke(nameof(ChangeBotStatus), UnityEngine.Random.Range(MinTimerChangeBotStatusAfterStartEvent, MaxTimerChangeBotStatusAfterStartEvent));

        if (!IsBotInDanger)
        {
            if (Biohazard.IsBotHuntForZombies && Bot != BotStatus.IsStoping && !IsBotHunt)
            {
                if (PlayerControl.PlayerTeam == Player.Team.Human)
                    Bot = BotStatus.IsRunning;
                IsBotHunt = true;
            }
            else if (!Biohazard.IsBotHuntForZombies && Bot != BotStatus.IsStoping && IsBotHunt)
            {
                if (PlayerControl.PlayerTeam == Player.Team.Human)
                    Bot = BotStatus.IsRunningShelter;
                IsBotHunt = false;
            }
/*
            if (Biohazard.IsBotGroupZombie && PlayerControl.PlayerTeam == Player.Team.Zombie && !IsBotFollow)
            {
                Bot = BotStatus.IsFollowPlayer;
                IsBotFollow = true;
            }
            else if (!Biohazard.IsBotGroupZombie && PlayerControl.PlayerTeam == Player.Team.Zombie && IsBotFollow)
            {
                ChangeBotStatus();
                IsBotFollow = false;
            }

            if (Biohazard.IsBotGroupHuman && PlayerControl.PlayerTeam == Player.Team.Human && !IsBotFollow)
            {
                Bot = BotStatus.IsFollowPlayer;
                IsBotFollow = true;
            }
            else if (!Biohazard.IsBotGroupHuman && PlayerControl.PlayerTeam == Player.Team.Human && IsBotFollow)
            {
                ChangeBotStatus();
                IsBotFollow = false;
            }*/
        }
    }

    private void BotWeaponsStateLogic()
    {
        if (IsBotInDanger && TargetAim)
        {
            switch (PlayerControl.PlayerTeam)
            {
                case Player.Team.Zombie:
                    {
                        if (Vector2.Distance(transform.position, TargetAim.transform.position) <= RadiusKnifeAttack)
                            WeaponControl.ClickButtonFireEnter();
                        else
                            WeaponControl.ClickButtonFireExit();
                        break;
                    }
                case Player.Team.Human:
                    {
                        if (PlayerControl.Weapons[PlayerControl.ActiveWeapon].Clip > 0)
                            WeaponControl.ClickButtonFireEnter();
                        else
                        {
                            if (WeaponControl.IsFire)
                            {
                                WeaponControl.ClickButtonFireExit();

                                if (!WeaponControl.IsDraw && UnityEngine.Random.Range(1, 101) < SuccessRateChangeWeapon)
                                    WeaponControl.ClickChangeWeapon(UnityEngine.Random.Range(0, 2));
                            }

                            if (!WeaponControl.IsDraw && !WeaponControl.IsReload && PlayerControl.Weapons[PlayerControl.ActiveWeapon].Ammo > 0)
                                WeaponControl.ClickButtonReloadEnter();
                        }

                        if (CurrentLeftTimeWeaponForReload < MinTimerWeaponForReload)
                            CurrentLeftTimeWeaponForReload = UnityEngine.Random.Range(MinTimerWeaponForReload, MaxTimerWeaponForReload);

                        if (CurrentLeftTimeChangeWeapon < MinTimerChangeWeapon)
                            CurrentLeftTimeChangeWeapon = UnityEngine.Random.Range(MinTimerChangeWeapon, MaxTimerChangeWeapon);

                        break;
                    }
            }
        }
        else
        {
            if (WeaponControl.IsFire)
                WeaponControl.ClickButtonFireExit();

            if (!WeaponControl.IsReload && !WeaponControl.IsDraw)
            {
                if (CurrentLeftTimeWeaponForReload >= 0)
                    CurrentLeftTimeWeaponForReload -= Time.deltaTime;

                if (CurrentLeftTimeChangeWeapon >= 0)
                    CurrentLeftTimeChangeWeapon -= Time.deltaTime;

                if (CurrentLeftTimeWeaponForReload < 0 && PlayerControl.Weapons[PlayerControl.ActiveWeapon].Clip < PlayerControl.Weapons[PlayerControl.ActiveWeapon].MaxClip && PlayerControl.Weapons[PlayerControl.ActiveWeapon].Ammo > 0)
                {
                    WeaponControl.ClickButtonReloadEnter();
                    CurrentLeftTimeWeaponForReload = UnityEngine.Random.Range(MinTimerWeaponForReload, MaxTimerWeaponForReload);
                }

                if (CurrentLeftTimeChangeWeapon < 0)
                {
                    if (UnityEngine.Random.Range(1, 101) < SuccessRateChangeWeapon)
                        WeaponControl.ClickChangeWeapon(UnityEngine.Random.Range(0, 2));

                    CurrentLeftTimeChangeWeapon = UnityEngine.Random.Range(MinTimerChangeWeapon, MaxTimerChangeWeapon);
                }
            }
        }
    }

    private IEnumerator BotDanger()
    {
        if(TargetAim)
            yield return new WaitForSeconds(UnityEngine.Random.Range(MinTimerDelayDetect, MaxTimerDelayDetect));
        else
            yield return new WaitForSeconds(0.05f);

        TargetAim = null;
        IsBotInDanger = false;

        Player TempTargetEnemy = TargetSearch.PlayerSearch(PlayerControl, RadiusPlayerDetect);

        if (TempTargetEnemy)
        {
            IsBotInDanger = true;
            TargetAim = TempTargetEnemy;

            switch (PlayerControl.PlayerTeam)
            {
                case Player.Team.Zombie:
                {
                    if (Bot != BotControl.BotStatus.IsStoping)
                        Bot = BotControl.BotStatus.IsRunningAndIsAttack;

                    break;
                }
                case Player.Team.Human:
                {
                    if (Bot != BotControl.BotStatus.IsStoping)
                    {
                        switch (Bot)
                        {
                            case BotControl.BotStatus.IsRunning: { Bot = BotControl.BotStatus.IsRunningAndIsAttack; break; }
                            case BotControl.BotStatus.IsRunningShelter: { Bot = BotControl.BotStatus.IsRunningShelterAndIsAttack; break; }
                            case BotControl.BotStatus.IsFollowPlayer: { Bot = BotControl.BotStatus.IsFollowPlayerAndIsAttack; break; }
                        }
                    }

                    break;
                }
            }
        }

        if(IsBotInDangerEscape && !TargetAim)
            IsBotInDangerEscape = false;

        CorBotDanger = null;
    }

    void OnBecameVisible()
    {
        if (gameObject && FeetAnim && IsOptimization)
        {
            FeetAnim.enabled = true;
            FeetSpr.color = new Color(255f, 255f, 255f, 255f);
            IsVisible = true;
        }
    }

    void OnBecameInvisible()
    {
        if (gameObject && FeetAnim && IsOptimization)
        {
            FeetAnim.enabled = false;
            FeetSpr.color = new Color(255f, 255f, 255f, 0f);
            IsVisible = false;
        }
    }
}
