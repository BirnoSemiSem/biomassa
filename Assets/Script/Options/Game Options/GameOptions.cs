﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOptions : MonoBehaviour
{
    public static GameOptions gameOptions;

    public GameObject Camera;
    public GameObject ParentBot;
    public GameObject ParentPlayer;

    public GameObject GameUI;
    public GameObject MenuUI;

    public LoadingMenu Loading;
    public ErrorMenu Error;
    public DialogMenu Dialog;

    // Start is called before the first frame update
    void Awake()
    {
        gameOptions = this;
    }
}
