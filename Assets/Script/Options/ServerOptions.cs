﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ServerOptions : MonoBehaviour
{
    public string ServerID;

    public List<MapController> MapControllers = new List<MapController>();
    public MapController SelectMap;

    public string[] Mode = { "рандом", "массовая атака", "чужие среди нас" };
    public string SelectMode;

    public string MapName;

    public GameObject CurrentMap = null;
    public int TimeLeftMinut;
    public float TimeLeftSeconds;

    public int CountBots = 0;

    public bool IsChangeMap = false;

    public void ServerOptionsStart()
    {
        if (SceneManager.GetActiveScene().path == SpecificalOptions.specificalOptions.Game)
        {

        }
    }

    public void LoadingMap()
    {
        if (SceneManager.GetActiveScene().path == SpecificalOptions.specificalOptions.Game)
        {
            for (int i = 0; i < MapControllers.Count; i++)
            {
                if (MapControllers[i] == SelectMap)
                {
                    CurrentMap = Instantiate(MapControllers[i].gameObject);

                    MapName = MapControllers[i].MapName;

                    TimeLeftMinut = MapControllers[i].MapTimeLimitMinut;
                    TimeLeftSeconds = 0;

                    break;
                }
            }
        }
    }

    void Update()
    {
        if (SceneManager.GetActiveScene().path == SpecificalOptions.specificalOptions.Game)
        {
            if (!IsChangeMap)
            {
                TimeLeftSeconds -= Time.deltaTime;

                if (TimeLeftSeconds <= 0)
                {
                    if (TimeLeftMinut > 0)
                    {
                        TimeLeftMinut--;
                        TimeLeftSeconds = 59;
                    }
                    else
                        IsChangeMap = true;
                }
            }
        }
    }

    void ChangeMap()
    {
        if(IsChangeMap)
        {

        }
    }
}
