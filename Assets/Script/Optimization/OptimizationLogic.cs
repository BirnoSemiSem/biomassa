﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptimizationLogic : MonoBehaviour
{
    public enum Effects
    {
        MuzzleFlash,
        Infection,
        Death,
        BloodPain,
        BloodFloor,
        Bullet
    }

    public static OptimizationLogic OptCode;

    //Effect Object
    public GameObject ParentEffect;

    //public GameObject MuzzleFlash;
    public GameObject Infection;
    public GameObject Death;
    public GameObject BloodPain;
    public GameObject BloodFloor;
    public GameObject Bullet;

    public int PooledAmout = 50;
    public bool WildGrow = true;

    //private List<GameObject> MuzzleFlashList = new List<GameObject>();
    private List<GameObject> InfectionList = new List<GameObject>();
    private List<GameObject> DeathList = new List<GameObject>();
    private List<GameObject> BloodPainList = new List<GameObject>();
    private List<GameObject> BloodFloorList = new List<GameObject>();
    private List<GameObject> BulletList = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        OptCode = this;

        for(int i = 0; i < PooledAmout; i++)
        {
            InfectionList.Add(CreateObj(Infection));
            DeathList.Add(CreateObj(Death));
            BloodPainList.Add(CreateObj(BloodPain));
            BloodFloorList.Add(CreateObj(BloodFloor));
            BulletList.Add(CreateObj(Bullet));
        }
    }

    public GameObject GetPoolObject(Effects effects)
    {
        switch(effects)
        {
            case Effects.Infection: return GetPoolObjectForList(InfectionList, effects);
            case Effects.Death: return GetPoolObjectForList(DeathList, effects);
            case Effects.BloodPain: return GetPoolObjectForList(BloodPainList, effects);
            case Effects.BloodFloor: return GetPoolObjectForList(BloodFloorList, effects);
            case Effects.Bullet: return GetPoolObjectForList(BulletList, effects);
        }
        return null;
    }

    private GameObject CreateObj(GameObject obj)
    {
        GameObject _temp = Instantiate(obj);
        _temp.transform.SetParent(ParentEffect.transform);
        _temp.SetActive(false);

        return _temp;
    }

    private GameObject GetPoolObjectForList(List<GameObject> Obj, Effects effects)
    {
        if(Obj.Count > 0)
        {
            for(int i = 0; i < Obj.Count; i++)
            {
                if(!Obj[i].activeInHierarchy)
                {
                    return Obj[i];
                }
            }
        }

        if(WildGrow)
        {
            GameObject obj;
            switch (effects)
            {
                case Effects.Infection:
                {
                    obj = CreateObj(Infection);
                    InfectionList.Add(obj);
                    return obj;
                }
                case Effects.Death:
                {
                    obj = CreateObj(Death);
                    DeathList.Add(obj);
                    return obj;
                }
                case Effects.BloodPain:
                {
                    obj = CreateObj(BloodPain);
                    BloodPainList.Add(obj);
                    return obj;
                }
                case Effects.BloodFloor:
                {
                    obj = CreateObj(BloodFloor);
                    BloodFloorList.Add(obj);
                    return obj;
                }
                case Effects.Bullet:
                {
                    obj = CreateObj(Bullet);
                    BulletList.Add(obj);
                    return obj;
                }
            }
        }
        return null;
    }
}
