﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DialogMenu : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] private GameObject DialogWindow;

    [SerializeField] private TextMeshProUGUI Text;

    [SerializeField] private Button ButtonYes;
    [SerializeField] private Button ButtonNo;

    [SerializeField] private LoadingMenu Loading;

    public void EscToGameExit()
    {
        if (!DialogWindow.activeInHierarchy)
        {
            DialogWindow.SetActive(true);
            ExitDialog("Вы действительно хотите выйти из игры?");
        }
        else
            DialogWindow.SetActive(false);
    }

    public void EscToMainMenu()
    {
        if (!DialogWindow.activeInHierarchy)
        {
            DialogWindow.SetActive(true);
            ExitToMainMenu("Вы действительно хотите вернуться в главное меню?");
        }
        else
            DialogWindow.SetActive(false);
    }

    public void ExitDialog(string str)
    {
        Text.text = str;

        ButtonYes.onClick.AddListener(ExitGame);
        ButtonNo.onClick.AddListener(DialogExitClose);
    }

    public void ExitToMainMenu(string str)
    {
        Text.text = str;

        ButtonYes.onClick.AddListener(ExitMainMenu);
        ButtonNo.onClick.AddListener(DialogExitClose);
    }

    //Function Button

    public void ExitGame()
    {
        Application.Quit();
    }

    public void ExitMainMenu()
    {
        DialogExitClose();

        SpecificalOptions.specificalOptions.IsChangeScene = true;

        if (SpecificalOptions.specificalOptions.IsHost)
            NetworkManager.singleton.StopHost();
        else if (SpecificalOptions.specificalOptions.IsOnlyClient)
            NetworkManager.singleton.StopClient();

        Loading.LoadingWindowUp("Загрузка");
        SceneManager.LoadSceneAsync(SpecificalOptions.specificalOptions.MainMenu);
    }

    public void DialogExitClose()
    {
        DialogWindow.SetActive(false);
    }
}
