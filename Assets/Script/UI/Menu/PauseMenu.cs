﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject Game;
    public GameObject Menu;
    public GameObject LoadingWindow;

    public static bool IsPauseMenu = false;

    //Menu
    [SerializeField] private GameObject MainMenu;
    [SerializeField] private GameObject SettingsMenu;
    [SerializeField] private GameObject SettingsAdvenceMenu;
    [SerializeField] private GameObject SettingsKeyControlMenu;
    [SerializeField] private GameObject Dialog;

    [SerializeField] private List<GameObject> Windows;

    //DialogWindow
    [SerializeField] private DialogMenu DialogWindow;

    void Start()
    {
        IsPauseMenu = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!Chat.IsChatActive)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (!Menu.activeInHierarchy)
                    OpenPauseMenu();
                else
                {
                    if (!MainMenu.activeInHierarchy || Dialog.activeInHierarchy)
                    {
                        if (Dialog.activeInHierarchy)
                            DialogWindow.EscToMainMenu();
                        else
                            DelWindows(Windows.Count - 1);
                    }
                    else
                        ClosePauseMenu();
                }
            }
        }
    }

    public void OpenPauseMenu()
    {
        Game.SetActive(false);
        LoadingWindow.SetActive(false);
        Menu.SetActive(true);
        MainMenu.SetActive(true);
        IsPauseMenu = true;
    }

    public void ClosePauseMenu()
    {
        Game.SetActive(true);
        Menu.SetActive(false);
        MainMenu.SetActive(false);
        LoadingWindow.SetActive(false);
        IsPauseMenu = false;
    }

    public void Back()
    {
        DelWindows(Windows.Count - 1);
    }

    public void ClickWindowSettingsMenu()
    {
        AddWindows(SettingsMenu, true);
    }

    public void ClickWindowSettingsAdvanceMenu()
    {
        AddWindows(SettingsAdvenceMenu, true);
    }

    public void ClickWindowSettingsKeyControlMenu()
    {
        AddWindows(SettingsKeyControlMenu, true);
    }

    void AddWindows(GameObject Window, bool IsCloseWindows)
    {
        if (MainMenu.activeInHierarchy && IsCloseWindows)
            MainMenu.SetActive(false);

        if (IsCloseWindows)
            for (int i = 0; i < Windows.Count; i++)
                if (Windows[i].activeInHierarchy)
                    Windows[i].SetActive(false);

        Windows.Add(Window);
        Window.SetActive(true);
    }

    void DelWindows(int index)
    {
        if (Windows.Count > 0)
        {
            if (Windows[index] != null)
            {
                Windows[index].SetActive(false);
                Windows.RemoveAt(index);
            }

            if (Windows.Count <= 0 && !MainMenu.activeInHierarchy)
                MainMenu.SetActive(true);
            else
                Windows[Windows.Count - 1].SetActive(true);
        }
    }

    public void OpenMainMenu()
    {
        DialogWindow.EscToMainMenu();
    }
}
