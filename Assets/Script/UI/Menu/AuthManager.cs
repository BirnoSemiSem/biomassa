﻿using Firebase;
using Firebase.Auth;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AuthManager : MonoBehaviour
{
    public DependencyStatus dependencyStatus;
    public FirebaseAuth auth;
    public FirebaseUser User;

    // Start is called before the first frame update
    [SerializeField] private GameObject LogInWindow;
    [SerializeField] private GameObject SignInWindow;

    [SerializeField] private TextMeshProUGUI EmailLogIn, PasswordLogIn;
    [SerializeField] private TextMeshProUGUI EmailSignIn, NickNameSignIn, PasswordSignIn;

    [SerializeField] private TextMeshProUGUI ErrorLogIn, ErrorSignIn;

    [SerializeField] private MainMenuControl MenuControl;
    [SerializeField] private GameObject MainMenu;


    void Start()
    {
        ErrorLogIn.text = "";
        ErrorSignIn.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Awake()
    {
        //Check that all of the necessary dependencies for Firebase are present on the system
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                //If they are avalible Initialize Firebase
                InitializeFirebase();
            }
            else
            {
                Debug.LogError("Could not resolve all Firebase dependencies: " + dependencyStatus);
            }
        });
    }

    private void InitializeFirebase()
    {
        Debug.Log("Setting up Firebase Auth");
        //Set the authentication instance object
        auth = FirebaseAuth.DefaultInstance;
    }

    public void SingIn()
    {
        LogInWindow.SetActive(false);
        SignInWindow.SetActive(true);
        ErrorLogIn.text = "";
        ErrorSignIn.text = "";
    }

    public void LogIn()
    {
        LogInWindow.SetActive(true);
        SignInWindow.SetActive(false);
        ErrorLogIn.text = "";
        ErrorSignIn.text = "";
    }

    public void SignUp()
    {
        PlayerInfoDelete();
        MenuControl.LoadingWindow.LoadingWindowUp("Подключение");
        StartCoroutine(Register(EmailSignIn.text, PasswordSignIn.text, NickNameSignIn.text));
    }

    public void LogUp()
    {
        PlayerInfoDelete();
        MenuControl.LoadingWindow.LoadingWindowUp("Подключение");
        StartCoroutine(Login(EmailLogIn.text, PasswordLogIn.text));
    }

    public void LogUpAutomatically(string Email, string Password)
    {
        StartCoroutine(Login(Email, Password));
    }

    void MainMenuStart()
    {
        MainMenu.SetActive(true);

        LogInWindow.SetActive(false);
        SignInWindow.SetActive(false);
    }

    static void PlayerInfoDataSave(string Email, string NickName, string Password)
    {
        SpecificalOptions.specificalOptions.PlayerInfo = new PlayerInfo();

        SpecificalOptions.specificalOptions.PlayerInfo.Email = System.Text.Encoding.UTF8.GetBytes(Email);
        SpecificalOptions.specificalOptions.PlayerInfo.NickName = System.Text.Encoding.UTF8.GetBytes(NickName);
        SpecificalOptions.specificalOptions.PlayerInfo.Password = System.Text.Encoding.UTF8.GetBytes(Password);

        DataSaver.saveData(SpecificalOptions.specificalOptions.PlayerInfo, "ProfilePlayer");
    }

    static void PlayerInfoDelete()
    {
        if (SpecificalOptions.specificalOptions.PlayerInfo != null)
        {
            DataSaver.deleteData("ProfilePlayer");
            SpecificalOptions.specificalOptions.PlayerInfo = null;
        }
    }

    private IEnumerator Login(string _email, string _password)
    {
        //Call the Firebase auth signin function passing the email and password
        var LoginTask = auth.SignInWithEmailAndPasswordAsync(_email, _password);
        //Wait until the task completes
        yield return new WaitUntil(predicate: () => LoginTask.IsCompleted);

        if (LoginTask.Exception != null)
        {
            //If there are errors handle them
            Debug.LogWarning(message: $"Failed to register task with {LoginTask.Exception}");
            FirebaseException firebaseEx = LoginTask.Exception.GetBaseException() as FirebaseException;
            AuthError errorCode = (AuthError)firebaseEx.ErrorCode;

            string message = "Login Failed!";
            switch (errorCode)
            {
                case AuthError.MissingEmail:
                    message = "Missing Email";
                    break;
                case AuthError.MissingPassword:
                    message = "Missing Password";
                    break;
                case AuthError.WrongPassword:
                    message = "Wrong Password";
                    break;
                case AuthError.InvalidEmail:
                    message = "Invalid Email";
                    break;
                case AuthError.UserNotFound:
                    message = "Account does not exist";
                    break;
            }

            MenuControl.LoadingWindow.LoadingWindowClear();

            if (!LogInWindow.activeInHierarchy)
                LogInWindow.SetActive(true);

            ErrorLogIn.text = message;
        }
        else
        {
            //User is now logged in
            //Now get the result
            User = LoginTask.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})", User.DisplayName, User.Email);

            if(SpecificalOptions.specificalOptions.PlayerInfo == null)
                PlayerInfoDataSave(User.Email, User.DisplayName, _password);

            MenuControl.LoadingWindow.LoadingWindowClear();
            ErrorSignIn.text = "";
            ErrorLogIn.text = "";
            MainMenuStart();
        }
    }

    private IEnumerator Register(string _email, string _password, string _username)
    {
        if (_username == "")
        {
            //If the username field is blank show a warning

            MenuControl.LoadingWindow.LoadingWindowClear();

            if (!SignInWindow.activeInHierarchy)
                SignInWindow.SetActive(true);

            ErrorSignIn.text = "Missing Username";
        }
        else
        {
            //Call the Firebase auth signin function passing the email and password
            var RegisterTask = auth.CreateUserWithEmailAndPasswordAsync(_email, _password);
            //Wait until the task completes
            yield return new WaitUntil(predicate: () => RegisterTask.IsCompleted);

            if (RegisterTask.Exception != null)
            {
                //If there are errors handle them
                Debug.LogWarning(message: $"Failed to register task with {RegisterTask.Exception}");
                FirebaseException firebaseEx = RegisterTask.Exception.GetBaseException() as FirebaseException;
                AuthError errorCode = (AuthError)firebaseEx.ErrorCode;

                string message = "Register Failed!";
                switch (errorCode)
                {
                    case AuthError.MissingEmail:
                        message = "Missing Email";
                        break;
                    case AuthError.MissingPassword:
                        message = "Missing Password";
                        break;
                    case AuthError.WeakPassword:
                        message = "Weak Password";
                        break;
                    case AuthError.EmailAlreadyInUse:
                        message = "Email Already In Use";
                        break;
                }

                MenuControl.LoadingWindow.LoadingWindowClear();

                if (!SignInWindow.activeInHierarchy)
                    SignInWindow.SetActive(true);

                ErrorSignIn.text = message;
            }
            else
            {
                //User has now been created
                //Now get the result
                User = RegisterTask.Result;

                if (User != null)
                {
                    //Create a user profile and set the username
                    UserProfile profile = new UserProfile { DisplayName = _username };

                    //Call the Firebase auth update user profile function passing the profile with the username
                    var ProfileTask = User.UpdateUserProfileAsync(profile);
                    //Wait until the task completes
                    yield return new WaitUntil(predicate: () => ProfileTask.IsCompleted);

                    if (ProfileTask.Exception != null)
                    {
                        //If there are errors handle them
                        Debug.LogWarning(message: $"Failed to register task with {ProfileTask.Exception}");
                        FirebaseException firebaseEx = ProfileTask.Exception.GetBaseException() as FirebaseException;
                        AuthError errorCode = (AuthError)firebaseEx.ErrorCode;

                        MenuControl.LoadingWindow.LoadingWindowClear();

                        if (!SignInWindow.activeInHierarchy)
                            SignInWindow.SetActive(true);

                        ErrorSignIn.text = "Username Set Failed!";
                    }
                    else
                    {
                        //Username is now set
                        //Now return to login screen

                        if (SpecificalOptions.specificalOptions.PlayerInfo == null)
                            PlayerInfoDataSave(User.Email, User.DisplayName, _password);

                        StartCoroutine(Login(User.Email, _password));

                        ErrorSignIn.text = "";
                    }
                }
            }
        }
    }
}
